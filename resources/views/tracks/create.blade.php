@extends('layouts.main')

@section('title', 'Create Tracks')

@section('content')

<div class="container-sm">
  <form method="POST" action="{{route('albums.tracks.store', $album)}}">
    @csrf
    <div class="form-group">
        <h3>Create Tracks</h3>
            <br>
            {{-- Styles List --}}
            <div class="form-group">
              <label for="autocomplete">Select a Styles: </label>
               <input type="text" id="style_name" class="form-control input-lg" placeholder="Enter Style" autocomplete="off" />
               <div id="stylesList">
               </div>
               
          <input type="hidden" name="style_id" id="style_id">
            @error('style_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <br>
  
      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" autocomplete="off">
      @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
    </div>

    <label for="name">Duration</label>
    {{-- Time --}}
    <div class="input-group mb-3">
      <div class="input-group-prepend">
        <span class="input-group-text">Hour</span>
      </div>

      <input type="number" class="form-control" placeholder="Insert Hours" id="hora" name="hora" value="{{ old('hora') }}">

      @error('hora')
        <p class="help is-danger" style="color: red">{{ $message }}</p>
        @enderror

      <div class="input-group-prepend">
        <span class="input-group-text">Minutes</span>
      </div>

      <input type="number" class="form-control" placeholder="Insert Minutes" id="minutos" name="minutos" value="{{ old('minutos') }}">

      @error('minutos')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror

      <div class="input-group-prepend">
        <span class="input-group-text">Seconds</span>
      </div>

      <input type="number" class="form-control" placeholder="Insert Seconds" id="segundos" name="segundos" value="{{ old('segundos') }}">

      @error('segundos')
        <p class="help is-danger" style="color: red">{{ $message }}</p>
        @enderror

    </div>
    

  
  <div class="modal-footer">
    <a href="{{ route('albums.tracks.index', $album) }}" class="float-right btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form>
  
</div>
    
@endsection

@section('scripts')

<script>
  $(document).ready(function(){
  
  $('#style_name').keyup(function(){ 
         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.styles') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            $('#stylesList').fadeIn();  
            $('#stylesList').html(data);
           }
          });
         }
     });
  
     $(document).on('click', 'li', function(){  
         $('#style_name').val($(this).text());  
         $('#stylesList').fadeOut();  
     });  
  
  });
  </script>
@endsection
