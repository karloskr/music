@extends('layouts.main')

@section('title', 'Show Track')

@section('content')

<div class="card">
    <h5 class="card-header">Show Track</h5>
    <div class="card-body">
        <form>
            @csrf
            <div class="form-group">
              
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Styles</label>
                    <input type="text" class="form-control" id="style_id" name="style_id" value="{{ old('style_id', $style->name) }}" disabled>
                </div>

              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $track->name) }}" disabled>
            </div>
            <label for="name">Duration</label>
              <input type="text" class="form-control" id="duration" name="duration" value="{{ old('duration', $track->duration) }}" disabled>
            </div>
          
          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('albums.tracks.index', $album)}}">Back</a>
          </div>
        </form>
    </div>
  </div>
    
@endsection