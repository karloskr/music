@extends('layouts.main')

@section('title', 'Edit Tracks')

@section('content')
<br>

<div class="form-group">
  <form method="POST" action="{{route('tracks.update',$track)}}">
    @csrf
    @method('PUT')
    <h2>Edit Track</h2>
    {{-- Styles List --}}
    <div class="form-group">
      <label for="autocomplete">Select a Styles: </label>
       <input type="text" id="style_name" class="form-control input-lg" placeholder="Enter Label Name" autocomplete="off" value="{{ $track->style->name }}"/>
       <div id="stylesList">
       </div>
      @csrf
  <input type="hidden" name="style_id" id="style_id" value="{{ $track->style->id }}">
    @error('style_id')
    <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror
    <br>

    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $track->name) }}" autocomplete="off">
    @error('name')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <br>
   {{-- Time --}}
   <label for="name">Duration</label>
   <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Hour</span>
    </div>

    <input type="number" class="form-control" placeholder="Insert Hours" id="hora" name="hora" value="{{ old('hora', $hours)}}">

    @error('hora')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror

    <div class="input-group-prepend">
      <span class="input-group-text">Minutes</span>
    </div>

    <input type="number" class="form-control" placeholder="Insert Minutes" id="minutos" name="minutos" value="{{ old('minutos', $minutes) }}">

    @error('minutos')
    <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <div class="input-group-prepend">
      <span class="input-group-text">Seconds</span>
    </div>

    <input type="number" class="form-control" placeholder="Insert Seconds" id="segundos" name="segundos" value="{{ old('segundos', $seconds) }}">

    @error('segundos')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror

  </div>
    
    <div class="modal-footer">
      <a href="{{ route('albums.tracks.index', $album) }}" class="float-right btn btn-success">Back</a>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  
</div>
  
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
  
  $('#style_name').keyup(function(){ 
         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.styles') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            $('#stylesList').fadeIn();  
            $('#stylesList').html(data);
           }
          });
         }
     });
  
     $(document).on('click', 'li', function(){  
         $('#style_name').val($(this).text());  
         $('#stylesList').fadeOut();  
     });  
  
  });
  </script>
@endsection