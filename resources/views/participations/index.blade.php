@extends('layouts.main')

@section('title', 'Participations')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><a href="{{ route('tracks.show', $track) }}">{{ $track->name }}</a> Participations</h1>
    <a href="{{ route('tracks.artist_tracks.create',$track) }}" class="btn btn-primary">Create</a>
</div> 

<div class="row">
    @include('custom.message')
</div>

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('albums.tracks.index', $album) }}">Tracks</a></li>
    <li class="breadcrumb-item" aria-current="page">Participation</li>
    <li class="breadcrumb-item active" aria-current="page">{{ $track->name}}</li>
  </ol>
</nav>

{{-- Search --}}
<nav class="navbar navbar-light float-right mb-4">

  <form class="form-inline">
    <input name="buscar" class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search" autocomplete="off">

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    <a href="{{route('tracks.artist_tracks.index', $track)}}" class="btn btn-danger my-2 my-sm-2" type="submit">Clear</a>
  </form>

</nav>

<!-- List participations -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Participations</th>
        <th scope="col">Artist</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($artist_tracks as $participation)
     <tr>
        <td scope="row">{{ $participation->relParticipation->id }}</td>
        <td scope="row">{{ $participation->relParticipation->name }}</td>
        <td scope="row">{{ $participation->relArtist->name }}</td>
        <td scope="row"><a href="{{ route('artist_tracks.edit', $participation) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $participation->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('artist_tracks.destroy', $participation) }}" id="formEli_{{ $participation->id}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $participation->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $artist_tracks->links() }}
  </span>

  <div class="modal-footer">
    <a href="{{ route('albums.tracks.index', $album) }}" class="float-left btn btn-success">Back</a>
  </div>

<!-- Modals -->
@include('participations.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){

            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

