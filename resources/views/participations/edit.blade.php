@extends('layouts.main')

@section('title', 'Update Participation')

@section('content')
<br>
<div class="container">
  <div class="row justify-content-center">
      <div class="col-md-8">
          <div class="card">
              <div class="card-header">
                  <h2>Edit Participation</h2>
              </div>
              <div class="card-body">
                <form method="POST" action="{{route('artist_tracks.update', $artist_track)}}">
                  @csrf
                  @method('PUT')
          
                  {{-- Participations --}}
                  <label for="exampleFormControlSelect1">List Participations</label>
                      <div class="input-group">
                        <div class="input-group-prepend">
                          <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                      </div>
                      <select name="participation_id" class="form-control">
                        <option class="hidden" selected disabled>Participations</option>
                        @foreach ($participations as $participation)
                          <option value="{{ $participation->id }}"
                            @if ($participation->id === $artist_track->participation_id)
                                selected
                            @endif
                            >
                            {{ $participation->name }}</option>
                        @endforeach
                    </select>
                  </div>
                  @error('participation_id')
                  <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror
                  <br>
                  {{-- Artists List --}}
                  <div class="form-group">
                    <input type="text" id="artist_name" class="form-control input-lg" placeholder="Enter Artist Name" autocomplete="off" value="{{ $artist_track->relArtist->name }}"/>
                    <div id="artistsList">
                    </div>
                   @csrf
               <input type="hidden" name="artist_id" id="artist_id" value="{{ $artists->id }}">
                 @error('artist_id')
                 <p class="help is-danger" style="color: red">{{ $message }}</p>
                 @enderror

                  <div class="modal-footer">
                    <a href="{{ route('tracks.artist_tracks.index', $track) }}" class="btn btn-success">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>

                </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

@section('scripts')
<script>
  artistas = [];
  $(document).ready(function(){
  $('#artist_name').keyup(function(){ 

         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.artists') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            artistas = data;
            lista = '<ul class="dropdown-menu" style="display:block; position:relative">';
            data.forEach(function(row){ 
            lista += '<li><a href="#" onclick="cambiarArtist(' + row.id +')">' + row.name + '</a></li>';
            });
            lista += '</ul>';
            $('#artistsList').fadeIn();  
            $('#artistsList').html(lista);
           }
          });
         }
     });
  });

  function cambiarArtist(id) {
    var artista = artistas.find(c => {
      return c.id == id;
    });

    $('#artist_name').val(artista.name);
    $('#artist_id').val(id);
    $('#artistsList').fadeOut();

  }
  </script>
@endsection