@extends('layouts.main')

@section('title', 'Info Bands')

@section('content')

<div class="jumbotron">
  <h1 class="display-4">Bands Catalogue</h1>
  {{-- <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
  <hr class="my-4">
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p> --}}



  <div class="navbar">
    <nav class="navbar navbar-light bg-light">
      <form class="form-inline" method="GET">
        
        <input class="form-control mr-sm-2" id="buscar" name="buscar" type="search" placeholder="Search" aria-label="Search" autocomplete="off" results=5>
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="{{ route('info-bands') }}">Clear</a>
      </form>

      
    </nav>
  </div>
</div>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Imagen</th>
      <th scope="col">Name</th>
      <th scope="col">Album</th>
      <th scope="col">Country</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($bands as $band)
    <tr>
      <th scope="row">{{ $band->id }}</th>
      <th scope="row">{{-- Imagenes --}}
        @if(isset($band->imagen) == 0)
        <img src="/img/no-image.png">
        @else
        <img src="{{ route('download_imagen', ['discs' => 'bands','imagen' => $band->imagen]) }}" alt="">
        @endif
      </th>
      <td scope="row"><a href="{{ route('show-bands', $band)}}">{{ $band->name }}</a></td>
      <td scope="row">
        @isset( $band->albums[0]->name )
          {{ $band->albums[0]->name }}
        @endisset
      </td>
      <td scope="row">{{ $band->country->name}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{{-- {{ $bands->links() }} --}}


@endsection



@section('css')
    <style>
      img{
        height: 70px;
        width: 70px;
      }
      
      @supports(object-fit: cover){
        img{
        height: 70px;
        object-fit: cover;
        object-position: center center;
      }

     
      button{
        margin: 3px;
      }
        
    </style>
@endsection