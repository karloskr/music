@extends('layouts.main')

@section('title', 'Band')

@section('content')

<section id="proyecto" class="projects">
  <div class="wrapper">
  <h2 class="title">Bands</h2>
  <article class="project">
    <div class="project-left">
        <h3 class="project-title">{{ $band->name }}</h3>
        <h4 class="project-language">{{$band->gender->name}}</h4>
        <p class="project-date"><strong>Duration: {{ $band->start }} to {{ $band->end }}</strong></p>
        <p class="project-link">Albums: {{ $band->albums->count() }}</p>
        <p class="project-description"><span>History:</span> {{ $band->history }}</p>
    </div>
    <figure class="project-container_image">
      @if(isset($band->imagen) == 0)
          <h3>No Image Available</h3>
          <span>Insert Image</span>
          @else
          <img class="project-image" src="{{ route('download_imagen', ['discs' => 'bands' ,'imagen' => $band->imagen]) }}" >
          @endif
    </figure>
  </article>
  </div>
</section>

<br>

<p>
  <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button" aria-expanded="false" aria-controls="multiCollapseExample1">Members</a>
 
</p>
<div class="row">
  <div class="col">
    <div class="collapse multi-collapse" id="multiCollapseExample1">
      <div>
        <table class="table table-striped">
          <thead>
            <tr>
              <th scope="col">Artists</th>
              <th scope="col">Start</th>
              <th scope="col">End</th>
              <th scope="col">Bands</th>

            </tr>
          </thead>
          <tbody>
            
              @foreach ($members as $member)
              <tr>
                <td ><a href="{{ route('show-artists', $member->artist_id)}}">{{$member->relArtist->name}}</a></td>
                <td >{{$member->init}}</td>
                <td >{{$member->end}}</td>
               <td>
                @foreach ($member->relArtist->bands as $band)
                <a href="{{ route('show-bands', $band->id)}}">{{$band->name}}</a>
            @if ( !$loop->last), @endif
        @endforeach
               </td>
              </tr>
            @endforeach
            
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>


<h3>Discography</h3>
<table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Imagen</th>
        <th scope="col">Name</th>
        <th scope="col">Year</th>
        <th scope="col">Style</th>
        <th scope="col">Label</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($albums as $album)
      <tr>
        <th scope="row">{{ $album->id }}</th>
        <th scope="row">{{-- Imagenes --}}
          @if(isset($album->imagen) == 0)
          <img style="height: 70px;" src="/img/no-image.png" class="round-circle">
          @else
          <img style="height: 70px;" src="{{ route('download_imagen', ['discs' => 'albums','imagen' => $album->imagen]) }}" alt="">
          @endif
        
        </th>
        <td><a href="{{ route('show-album',$album) }}">{{ $album->name }}</a></td>
        <td>{{ $album->year }}</td>
        <td>
          {{ implode(", ",$album->styles->pluck('name')->all())  }}
        </td>
        <td>{{ $album->labels->name }}</td>
      </tr>
      @endforeach
      
    </tbody>
    
  </table>
  
  {{ $albums->links() }}
@endsection


@section('css')
    <style>
      
.projects{
    padding: 0px 30px 10px 30px;
    /* background-color: #744bc525; */
    background-color: white;
}
.project{
    border-radius: 10px;
    box-shadow: 0px 0px 10px black;
    /*right - down - difuminado - color*/
    padding: 30px 0px 30px 30px;
    background-color: white;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0px 0px 30px 0px;
}
.project-left{
    width: 500px;
}
.project-title{
    margin: 0px;
    font-size: 25px;
    margin-bottom: 10px;
}
.project-language{
    margin-top: 20px;
}
.project-date{
    margin-top: 20px;
}
.project-link{
    margin-top: 20px;
}
.project-link a{
    text-decoration: none;
    color: black;
}
.project-description{
    margin-top: 20px;
    font-size: 20px;
}
.project-container_image{
    width: 400px;
    margin: 0px 20px;
}
.project-image{
    width: inherit;
}

@supports(object-fit: cover){
  .project-image{
        height: inherit;
        object-fit: cover;
        object-position: center center;
      }
      
.title{
  font-size: 30px;
  padding: 5px;
}
    </style>
@endsection