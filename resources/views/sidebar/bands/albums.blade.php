@extends('layouts.main')

@section('title', 'Info Album')

@section('content')
<section id="proyecto" class="projects">
  <div class="wrapper">
  <h2 class="title">{{$album->bands->name}}: {{ $album->name }}</h2>
  <article class="project">
    <div class="project-left">
        <table class="table table-striped">
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">List of Tracks</th>
                <th scope="col">Duration</th>
              </tr>
            </thead>
            <tbody>
              @foreach ($tracks as $track)
              <tr>
                <th scope="row">{{ $id++ }}</th>
                <td>
                  <a data-toggle="collapse" href="#multiCollapseExample1-{{$track->id}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1-{{$track->id}}">
                    {{$track->name}}
                  </a>
                  <div class="row">
                    <div class="col">
                      <div class="collapse multi-collapse" id="multiCollapseExample1-{{$track->id}}">
                        <div>
                          @foreach ($track->relArtistsTracks as $art)
                              <ul>
                                <li style="list-style: none">
                                  {{ $art->relParticipation->name }}
                                  :
                                  {{ $art->relArtist->name }}
                                </li>
                              </ul>
                          @endforeach
                        </div>
                      </div>
                    </div>
                  </div>
                </td>
                <td>{{ $track->duration_format }}</td>              
              </tr>
              @endforeach
              <tr>
                <td>Total</td>
                <td></td>
                <td>{{ date('H:i:s', $var) }}</td>
              </tr>
            </tbody>
          </table>
    </div>
    <div>

      <figure class="project-container_image">
        <img class="project-image" src="{{ route('download_imagen', ['discs' => 'albums','imagen' => $album->imagen]) }}" >
      </figure>

    </div>

  </article>
  </div>
</section>
<br>
<h3>Members</h3>
<br>
<table class="table table-striped">
  <thead>
    <tr>

      <th scope="col">Artists</th>
      <th scope="col">Contribution</th>
 
    </tr>
  </thead>
  <tbody>
    @foreach ($members as $member)
    <tr>
          <td scope="row"><a href="{{ route('show-artists', $member->id)}}">{{ $member->name}}</a></td>

          <td>{{ implode(", ",$member->relArtistAlbum->pluck('relParticipation')->pluck('name')->all())  }}</td>
          
    </tr>
    @endforeach
    
  </tbody>
</table>

@endsection


@section('css')
    <style>
      
.projects{
    padding: 0px 30px 10px 30px;
    /* background-color: #744bc525; */
    background-color: white;
}
.project{
    border-radius: 10px;
    box-shadow: 0px 0px 10px black;
    /*right - down - difuminado - color*/
    padding: 30px 0px 30px 30px;
    background-color: white;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0px 0px 30px 0px;
}
.project-left{
    width: 500px;
}
.project-title{
    margin: 0px;
    font-size: 25px;
    margin-bottom: 10px;
}
.project-language{
    margin-top: 20px;
}
.project-date{
    margin-top: 20px;
}
.project-link{
    margin-top: 20px;
}
.project-link a{
    text-decoration: none;
    color: black;
}
.project-description{
    margin-top: 20px;
    font-size: 20px;
}
.project-container_image{
    width: 400px;
    margin: 0px 20px;
}
.project-image{
    width: inherit;
}

.credits{
  margin: 20px;
}

.title{
  font-size: 30px;
  padding: 5px;
}

    </style>
@endsection