@extends('layouts.main')

@section('title', 'Artist')

@section('content')

{{-- Tab --}}
  <nav>
    <div class="nav nav-tabs" id="nav-tab" role="tablist">
      <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Home</a>
      <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Bands</a>
      <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Albums</a>
    </div>
  </nav>
  <section id="proyecto" class="projects">
  <div class="tab-content" id="nav-tabContent">
    {{-- 1 Tab--}}
    <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
      <div class="wrapper">
        <h2 class="title">Artist</h2>
        <article class="project">
          <div class="project-left">
              <h3 class="project-title">{{ $artist->name }}</h3>
              {{-- <h4 class="project-language">Genero</h4> --}}
              <p class="project-date"><strong>Day of Birth: {{ $artist->day_birth }}</strong></p>
              <p class="project-link">Bands: {{ $artist->albums->count() }}</p>
              <p class="project-description"><span>Profile:</span> {{ $artist->profile }}</p>
          </div>
          <figure class="project-container_image">
            @if(isset($artist->imagen) == 0)
                <h3>No Image Available</h3>
                <span>Insert Image</span>
                @else
                <img class="project-image" src="{{ route('download_imagen', ['discs' => 'artists' ,'imagen' => $artist->imagen]) }}" >
                @endif
          </figure>
        </article>
      </div>
    </div>{{-- End Tab 1 --}}
    {{-- 2 Tab --}}
      <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
       
          <h2 class="title">Bands</h2>  
          @foreach ($bands as $band)
          <p>
            <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1-{{$band->id}}" role="button" aria-expanded="false" aria-controls="multiCollapseExample1-{{$band->id}}">{{$band->name}}</a>
          </p>

          <div class="row">
            <div class="col">
              <div class="collapse multi-collapse" id="multiCollapseExample1-{{$band->id}}">
                <div class="card card-body">
                  <div class="row row-cols-2">

                    @foreach ($band->albums as $album)
                      <div class="card mb-3" style="max-width: 520px;">
                        <div class="row no-gutters">
                          <div class="col-sm-4 img">
                            @if(isset($album->imagen) == 0)
                              <img style="height: 160px;" src="/img/no-image.png">
                              @else
                              <img style="height: 160px;" src="{{ route('download_imagen', ['discs' => 'albums','imagen' => $album->imagen]) }}" alt="">
                              @endif
                          </div>
                          <div class="col-sm-4">
                            <div class="card-body">
                              <h5 class="card-title"><a href="{{ route('show-album',$album) }}">{{ $album->name }} - {{ $album->year }}</a></h5>
                              
                              <p class="card-text">Band: <small class="text-muted"><a href="{{ route('show-bands', $album->bands->id)}}">{{$album->bands->name}}</a></small></p>
                            </div>
                          </div>
                        </div>
                      </div> 
                    @endforeach
          
                  </div>
                </div>
              </div>
            </div>
          </div>
          @endforeach        
      </div>{{-- End Tab 2 --}}
    </div>
  
</section>

@endsection


@section('css')
    <style>
      
.projects{
    padding: 0px 30px 10px 30px;
    /* background-color: #744bc525; */
    background-color: white;
}
.project{
    border-radius: 10px;
    box-shadow: 0px 0px 10px black;
    /*right - down - difuminado - color*/
    padding: 30px 0px 30px 30px;
    background-color: white;
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin: 0px 0px 30px 0px;
}
.project-left{
    width: 500px;
}
.project-title{
    margin: 0px;
    font-size: 25px;
    margin-bottom: 10px;
}
.project-language{
    margin-top: 20px;
}
.project-date{
    margin-top: 20px;
}
.project-link{
    margin-top: 20px;
}
.project-link a{
    text-decoration: none;
    color: black;
}
.project-description{
    margin-top: 20px;
    font-size: 20px;
}
.project-container_image{
    width: 400px;
    margin: 0px 20px;
}
.project-image{
    width: inherit;
}

@supports(object-fit: cover){
  .project-image{
        height: inherit;
        object-fit: cover;
        object-position: center center;
      }

.dimension{
  width: 500px;
  height: 300px;
}

nav{
  margin:0px;
  padding:5px;
  
}

.title{
  font-size: 30px;
  padding: 5px;
}

</style>
@endsection