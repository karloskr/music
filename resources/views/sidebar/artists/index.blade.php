@extends('layouts.main')

@section('title', 'Info Artists')

@section('content')


<div class="jumbotron">
  <h1 class="display-4">Artists Catalogue</h1>
  {{-- <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
  <hr class="my-4">
  <p>It uses utility classes for typography and spacing to space content out within the larger container.</p> --}}



  <div class="navbar">
    <nav class="navbar navbar-light bg-light">
      <form class="form-inline" method="GET">
        
        <input class="form-control mr-sm-2" id="buscar" name="buscar" type="search" placeholder="Search" aria-label="Search" autocomplete="off">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        <a class="btn btn-outline-danger my-2 my-sm-0" type="submit" href="{{ route('info-artists') }}">Clear</a>
      </form>

      
    </nav>
  </div>
</div>

<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Imagen</th>
      <th scope="col">Name</th>
      <th scope="col">Bands</th>
      <th scope="col">Country</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($artists as $artist)
    <tr>
      <th scope="row">{{ $artist->id }}</th>
      <th scope="row">{{-- Imagenes --}}
        @if(isset($artist->imagen) == 0)
        {{-- <img  src="/img/no-image.png"> --}}
        Insert Image
        @else
        <img  src="{{ route('download_imagen', ['discs' => 'artists','imagen' => $artist->imagen]) }}" alt="">
        @endif
      </th>
      <td scope="row"><a href="{{ route('show-artists', $artist)}}">{{ $artist->name }}</a></td>
      <td scope="row">{{ $artist->bands_count }}</td>
      <td scope="row">{{ $artist->country->name}}</td>
    </tr>
    @endforeach
  </tbody>
</table>
{{ $artists->links() }}


@endsection



@section('css')
    <style>
      img{
        height: 70px;
        width: 70px;
      }
      
      @supports(object-fit: cover){
        img{
        height: 70px;
        object-fit: cover;
        object-position: center center;
      }
      
      button{
        margin: 3px;
      }
    </style>
@endsection