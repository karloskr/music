@extends('layouts.main')

@section('title', 'Show Style')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Styles</h1>

</div> 

<div class="card">
    <h5 class="card-header">Show Style</h5>
    <div class="card-body">
      <h3 class="card-text">{{ $style->name }}</h3>
    </div>
  </div>
  <br>
  <a href="{{ route('genders.styles.index', $gender) }}" class="float-right btn btn-success">Back</a>
@endsection