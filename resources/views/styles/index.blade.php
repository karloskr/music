@extends('layouts.main')

@section('title', 'Styles')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Styles</h1>
    <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#modalCreate"><i
            class="fas fa-user fa-sm text-white-50"></i>Create</button>
</div> 

<nav class="navbar navbar-light float-right mb-4">

  <form class="form-inline">
    <input name="buscar" class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search">

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    <a href="{{route('genders.styles.index', $gender)}}" class="btn btn-danger my-2 my-sm-2" type="submit">Clear</a>
  </form>

</nav>

<div class="row">
    @include('custom.message')
</div>

<!-- List genders -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Styles</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($styles as $style)
     <tr>
        <td scope="row">{{ $style->id }}</td>
        <td scope="row"><a href="{{ route('styles.show', $style->id) }}">{{ $style->name }}</a></td>
        <td scope="row"><a href="{{ route('styles.edit', $style) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $style->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('styles.destroy', $style) }}" id="formEli_{{ $style->id}}" method="POST">
            @csrf
           <!--  @method('DELETE') -->
            <input type="hidden" name="id" value="{{ $gender->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $styles->links() }}
  </span>
  <a href="{{ route('genders.index') }}" class="float-right btn btn-success">Back</a>
  
<!-- Modals -->
@include('styles.modalCreate')
@include('styles.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            @if($message = session('ErrorInsert'))
                $('#modalCreate').modal('show');
            @endif
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

