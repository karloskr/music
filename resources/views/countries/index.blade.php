@extends('layouts.main')

@section('title', 'Countries')

@section('content')

    <div class="container box">
        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Countries</h1>
            <a class="btn btn-primary" data-toggle="modal" data-target="#modalCreate">Create</a>
        </div> 

        <div class="form-group">
        <label for="autocomplete">Select a Country: </label>
         <input type="text" name="country_name" id="country_name" class="form-control input-lg" placeholder="Enter Country Name" autocomplete="off"/>
         <div id="countryList">
         </div>

        @csrf
       </div>

@endsection

@section('scripts')
       {{-- Libreria JQuery --}}
    

    <script>
$(document).ready(function(){

$('#country_name').keyup(function(){ 
       var query = $(this).val();
       if(query != '')
       {
        var _token = $('input[name="_token"]').val();
        $.ajax({
         url:"{{ route('search.fetch') }}",
         method:"POST",
         data:{query:query, _token:_token},
         success:function(data){
          $('#countryList').fadeIn();  
                   $('#countryList').html(data);
         }
        });
       }
   });

   $(document).on('click', 'li', function(){  
       $('#country_name').val($(this).text());  
       $('#countryList').fadeOut();  
   });  

});

  </script>
@endsection

