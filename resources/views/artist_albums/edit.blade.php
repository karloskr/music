@extends('layouts.main')

@section('title', 'Edit artist_album')

@section('content')
<br>

<div class="card">
    <h5 class="card-header">Edit Artist_Album</h5>
    <div class="card-body">
        <form method="POST" action="{{ route('artist_albums.update',$artistAlbum)}}">
            @csrf
            @method('PUT')
            <div class="form-group">
              <label for="autocomplete">Select a Artist: </label>
               <input type="text" id="artist_name" class="form-control input-lg" placeholder="Enter Artist Name" autocomplete="off" value="{{ $artistasyAlbum->name }}"/>
               <div id="artistsList">
               </div>
              @csrf
          <input type="hidden" name="artist_id" id="artist_id" value="{{ $artistasyAlbum->id }}">
          @error('artist_id')
          <p class="help is-danger" style="color: red">{{ $message }}</p>
          @enderror


          {{-- Participations --}}
          <label for="exampleFormControlSelect1">List Participations</label>
          <div class="input-group">
            <div class="input-group-prepend">
              <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
          </div>
          <select name="participation_id" class="form-control">
            <option class="hidden" selected disabled>Participations</option>
            @foreach ($participations as $participation)
              <option value="{{ $participation->id }}"
                @if ($participation->id === $artistAlbum->participation_id)
                    selected
                @endif
                >
                {{ $participation->name }}</option>
            @endforeach
        </select>

            </div>
          
          <div class="modal-footer">
            <a href="{{ route('albums.artist_albums.index',$album) }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
</div>
  
</div>
  
@endsection

@section('scripts')
<script>
  artistas = [];
  $(document).ready(function(){
  $('#artist_name').keyup(function(){ 

         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.artists') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            artistas = data;
            lista = '<ul class="dropdown-menu" style="display:block; position:relative">';
            data.forEach(function(row){ 
            lista += '<li><a href="#" onclick="cambiarArtist(' + row.id +')">' + row.name + '</a></li>';
            });
            lista += '</ul>';
            $('#artistsList').fadeIn();  
            $('#artistsList').html(lista);
           }
          });
         }
     });
  });

  function cambiarArtist(id) {
    var artista = artistas.find(c => {
      return c.id == id;
    });

    $('#artist_name').val(artista.name);
    $('#artist_id').val(id);
    $('#artistsList').fadeOut();

  }
  </script>
@endsection