@extends('layouts.main')

@section('title', 'Artist-Album')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><a href="{{-- {{ route('bands.show',$band) }} --}}">{{-- {{ $band->name }} --}}</a> Artist-Album</h1>
    <a href="{{ route('albums.artist_albums.create',$album) }}" class="btn btn-primary">Create</a>
</div> 

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('bands.albums.index',$band) }}">Album</a></li>
    <li class="breadcrumb-item" aria-current="page">Artist-Album</li>
    <li class="breadcrumb-item active" aria-current="page">{{-- {{ $band->name}} --}}</li>
  </ol>
</nav>

<nav class="navbar navbar-light float-right mb-4">

  <form class="form-inline">

    <input name="buscar" class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search" autocomplete="off">

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    <a href="{{-- {{route('bands.albums.index', $band)}} --}}" class="btn btn-danger my-2 my-sm-2" type="submit">Clear</a>
  </form>

</nav>

<div class="row">
    @include('custom.message')
</div>

<!-- List albums -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Artist</th>
        <th scope="col">Contribution</th>
      </tr>
    </thead>
    <tbody>
     @foreach ($artistasyAlbums as $artist_album)
     <tr>
        <td scope="row">{{ $artist_album->id }}</td>
        <td scope="row">{{ $artist_album->relArtist->name }}</td>
        <td scope="row">{{ $artist_album->relParticipation->name }}</td>
        <td scope="row"><a href="{{ route('artist_albums.edit',$artist_album) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $artist_album->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('artist_albums.destroy', $artist_album) }}" id="formEli_{{ $artist_album->id}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{-- {{ $album->id }} --}}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $artistasyAlbums->links() }}
  </span>

<div class="modal-footer">
  <a href="{{ route('bands.albums.index',$band) }}" class="float-left btn btn-success">Back</a>
</div>
<!-- Modals -->
{{-- @include('albums.modalCreate') --}}
@include('albums.modalDelete')
<!-- EndModals -->


@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

