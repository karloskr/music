@extends('layouts.main')

@section('title', 'Create Artist')

@section('content')

<div class="card">
    <h5 class="card-header">Create Artist</h5>
    <div class="card-body">
        <form method="POST" action="{{route('albums.artist_albums.store',$album)}}">
            @csrf

            <div class="form-group">
              <label for="autocomplete">Select a Artist: </label>
               <input type="text" id="artist_name" class="form-control input-lg" placeholder="Enter Artist Name" autocomplete="off" />
               <div id="artistsList">
               </div>
              @csrf
          <input type="hidden" name="artist_id" id="artist_id">
          @error('artist_id')
          <p class="help is-danger" style="color: red">{{ $message }}</p>
          @enderror

        <br>
          <h3>Create participations</h3>
          <br>
      {{-- Participations --}}
        <div class="input-group">
          <div class="input-group-prepend">
            <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
        </div>
        <select name="participation_id" class="form-control">
          <option class="hidden" selected disabled>Participations</option>
          @foreach ($participations as $participation)
            <option value="{{ $participation->id }}">
              {{ $participation->name }}</option>
          @endforeach
      </select>
    </div>
    @error('participation_id')
    <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror
</div>
            
          
          <div class="modal-footer">
            <a href="{{ route('albums.artist_albums.index',$album) }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
</div>
    
@endsection
@section('scripts')
       {{-- Libreria JQuery --}}
    
       <script>
        $(document).ready(function(){
        
        $('#artist_name').keyup(function(){ 
               var query = $(this).val();
               if(query != '')
               {
                var _token = $('input[name="_token"]').val();
                $.ajax({
                 url:"{{ route('search.artists') }}",
                 method:"POST",
                 data:{query:query, _token:_token},
                 success:function(data){
                  var lista = '<ul class="dropdown-menu" style="display:block; position:relative">';
                  data.forEach(function(row){ 
                    lista += '<li><a href="#" onclick="cambiarArtist(' + row.id +', `' + row.name + '`)">' + row.name + '</a></li>';
                  });
                  lista += '</ul>';
                  $('#artistsList').fadeIn();  
                  $('#artistsList').html(lista);
                 }
                });
               }
           });
        });
      
        function cambiarArtist(id, name) {
          $('#artist_name').val(name);
          $('#artist_id').val(id);
          $('#artistsList').fadeOut();
          
        }
      
        </script>
@endsection