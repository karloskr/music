@extends('layouts.main')

@section('title', 'Show artist')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
  <h1 class="h3 mb-0 text-gray-800">Show Artist</h1>
</div> 

<div class="card">
  {{-- <h5 class="card-header">Show Artist</h5> --}}
  <div class="card-body">
   
    <label for="name">Name</label>
            <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $artist->name) }}" disabled>
          <label for="date_birth">Birth</label>
            <input type="date" class="form-control" id="birth" name="day_birth" value="{{ old('day_birth', $artist->day_birth) }}" disabled>
          <label for="date_birth">Death</label>
            <input type="date" class="form-control" id="death" name="day_death" value="{{ old('year', $artist->day_death) }}" disabled>
          
            <div class="mb-3">
              <label for="profile">Profile</label>
              <textarea class="form-control" id="profile" name="profile" disabled>{{ $artist->profile }}</textarea>
            </div>
            <label for="exampleFormControlSelect1">Countries</label>
            <select disabled class="form-control" id="countries" name="country_id">
                @foreach ($countries as $country)
                  <option value="{{ $country->id }}"
                    @if ($country->id === $artist->country_id)
                        selected
                    @endif
                    >
                    {{ $country->name }}</option>
                @endforeach
            </select>

          </div>
        
        <div class="modal-footer">
          <a type="button" class="btn btn-success" href="{{ route('artists.index') }}">Back</a>
        </div>
  </div>
</div>
@endsection