@extends('layouts.main')

@section('title', 'Create Artist')

@section('content')

<div class="card">
    <h5 class="card-header">Create Artist</h5>
    <div class="card-body">
        <form method="POST" action="{{route('artists.store')}}" enctype="multipart/form-data">
            @csrf

            <div class="form-group">
              <label for="autocomplete">Select a Country: </label>
               <input type="text" id="country_name" class="form-control input-lg" placeholder="Enter Country Name" autocomplete="off" />
               <div id="countryList">
               </div>
              @csrf
          <input type="hidden" name="country_id" id="country_id">

            <div class="form-group">
              <label for="name">Name Artists</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" autocomplete="off">
              @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="profile">Profile</label>
          <textarea class="form-control" id="profile" name="profile" value="{{ old('profile') }}"></textarea>
          @error('profile')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="name">Birth</label>
              <input type="date" class="form-control" id="birth" name="day_birth" value="{{ old('day_birth') }}">
              @error('day_birth')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
              <label for="name">Death</label>
              <input type="date" class="form-control" id="death" name="day_death" value="{{ old('day_death') }}">
              @error('day_death')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror

              <br>
              <div class="form-group">
                <label for="imagenes">Images</label>
                <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
              </div>
              
        <br>
  
              
            </div>
          
          <div class="modal-footer">
            <a href="{{ route('artists.index') }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
</div>
    
@endsection
@section('scripts')
       {{-- Libreria JQuery --}}
    
<script>
$(document).ready(function(){

$('#country_name').keyup(function(){ 
       var query = $(this).val();
       if(query != '')
       {
        var _token = $('input[name="_token"]').val();
        $.ajax({
         url:"{{ route('search.fetch') }}",
         method:"POST",
         data:{query:query, _token:_token},
         success:function(data){
          $('#countryList').fadeIn();  
                   $('#countryList').html(data);
         }
        });
       }
   });

   $(document).on('click', 'li', function(){  
       $('#country_name').val($(this).text());  
       $('#countryList').fadeOut();  
   });  

});
</script>
@endsection