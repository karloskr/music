@extends('layouts.main')

@section('title', 'Update Artist')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Artist</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('artists.update', $artist)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                      <input type="text" id="country_name" class="form-control input-lg" placeholder="Country Name" 
                      autocomplete="off" value="{{ $artist->country->name }}"/>
                      <div id="countryList">
                      </div>
                     @csrf
                  <input type="hidden" name="country_id" id="country_id" value="{{ $artist->country->id }}">

                   @error('country_id')
                   <p class="help is-danger" style="color: red">{{ $message }}</p>
                   @enderror

                   <br>

                  <label for="profile">Name</label>
                  <input class="form-control" id="artist" name="name" value="{{ old('name', $artist->name) }}" autocomplete="off">
                    <br>
                 @error('name')
                 <p class="help is-danger" style="color: red">{{ $message }}</p>
                 @enderror
                    <br>
                      <label for="profile">Profile</label>
                  <textarea class="form-control" id="profile" name="profile" value="{{ old('profile') }}">{{ $artist->profile }}</textarea>
                    <br>
                      <label for="name">Birth</label>
                      <input type="date" class="form-control" id="birth" name="day_birth" value="{{ old('day_birth', $artist->day_birth) }}">
                      <br>
                      <label for="name">Death</label>
                      <input type="date" class="form-control" id="death" name="day_death" value="{{ old('day_death', $artist->day_death) }}">

                <br>
                      <div class="form-group">
                        <label for="imagenes">Images</label>
                        <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
                      </div>
                      
                <br>
                    </div>

                    
                    <a type="button" class="btn btn-success" href="{{ route('artists.index') }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
       {{-- Libreria JQuery --}}
    
<script>
$(document).ready(function(){

$('#country_name').keyup(function(){ 
       var query = $(this).val();
       if(query != '')
       {
        var _token = $('input[name="_token"]').val();
        $.ajax({
         url:"{{ route('search.fetch') }}",
         method:"POST",
         data:{query:query, _token:_token},
         success:function(data){
          $('#countryList').fadeIn();  
                   $('#countryList').html(data);
         }
        });
       }
   });

   $(document).on('click', 'li', function(){  
       $('#country_name').val($(this).text());  
       $('#countryList').fadeOut();  
   });  

});

</script>
@endsection


