@extends('layouts.main')

@section('title', 'Labels')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Labels</h1>
    
    <a class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#modalCreate">
      <i class="fas fa-user fa-sm text-white-50"></i>Create</a>
</div> 

<nav class="navbar navbar-light float-right mb-4">

  <form class="form-inline">
    <input name="buscar" class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search">

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    <a href="{{route('labels.index')}}" class="btn btn-danger my-2 my-sm-2" type="submit">Clear</a>
  </form>

</nav>  

<div class="row">
    @include('custom.message')
</div>

<!-- List labels -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Labels</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($labels as $label)
     <tr>
        <td scope="row">{{ $label->id }}</td>
        <td scope="row"><a href="{{ route('labels.show', $label->id) }}">{{ $label->name }}</a></td>
        <td scope="row"><a href="{{ route('labels.edit', $label) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $label->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('labels.destroy', $label) }}" id="formEli_{{ $label->id}}" method="POST">
            @csrf
           <!--  @method('DELETE') -->
            <input type="hidden" name="id" value="{{ $label->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $labels->links() }}
  </span>

<!-- Modals -->
@include('labels.modalCreate')
@include('labels.modalDelete')
<!-- EndModals -->

@endsection


@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            @if($message = session('ErrorInsert'))
                $('#modalCreate').modal('show');
            @endif
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

