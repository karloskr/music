@extends('layouts.main')

@section('title', 'Create Album')

@section('content')
    

<div class="container-sm">
  <form method="POST" action="{{route('bands.albums.store', $band)}}" enctype="multipart/form-data">
    @csrf
    <div class="form-group">            
            <div class="form-group">
              <label for="autocomplete">Select a Label: </label>
               <input type="text" id="label_name" class="form-control input-lg" placeholder="Enter Label Name" autocomplete="off" />
               <div id="albumsList">
               </div>
              @csrf
          <input type="hidden" name="label_id" id="label_id">
            @error('label_id')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror
            <br>

      <label for="name">Name</label>
      <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" autocomplete="off">
      @error('name')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
  
      <label for="name">Year</label>
      <input type="number" class="form-control" id="year" name="year" value="{{ old('year') }}">
      @error('year')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
    </div>
<br>
      <div class="form-group">
        <label for="imagenes">Images</label>
        <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
      </div>
      
<br>
    <label for="myCheck">Styles:</label>
    <input type="checkbox" id="myCheck" onclick="myFunction()">

      <div class="container" id="text" style="display:none">
        <div class="row row-cols-3">
          @foreach ($styles as $style)
            <div class="col"> <input type="checkbox" name="style[]" id="style_{{ $style->id }}" value="{{ $style->id }}"
              @if (is_array(old('style')) && in_array("$style->id", old('style'))  )
                checked
            @endif
              > {{ $style->name }}</div>
          @endforeach
        </div>
      </div>
    <br>
</div>
  
  
  <div class="modal-footer">
    <a href="{{ route('bands.albums.index', $band) }}" class="float-right btn btn-success">Back</a>
    <button type="submit" class="btn btn-primary">Save changes</button>
  </div>
  </form> 
</div>
    
@endsection

@section('scripts')
  <script>
    function myFunction() {
      var checkBox = document.getElementById("myCheck");
      var text = document.getElementById("text");
      if (checkBox.checked == true){
        text.style.display = "block";
      } else {
        text.style.display = "none";
      }
    }
    </script>
    <script>
      $(document).ready(function(){
      
      $('#label_name').keyup(function(){ 
             var query = $(this).val();
             if(query != '')
             {
              var _token = $('input[name="_token"]').val();
              $.ajax({
               url:"{{ route('search.label') }}",
               method:"POST",
               data:{query:query, _token:_token},
               success:function(data){
                $('#albumsList').fadeIn();  
                $('#albumsList').html(data);
               }
              });
             }
         });
      
         $(document).on('click', 'li', function(){  
             $('#label_name').val($(this).text());  
             $('#albumsList').fadeOut();  
         });  
      
      });
      </script>
@endsection
