@extends('layouts.main')

@section('title', 'Albums')

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><a href="{{ route('bands.show',$band) }}">{{ $band->name }}</a> Albums</h1>
    <a href="{{ route('bands.albums.create', $band) }}" class="btn btn-primary">Create</a>
</div> 

<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{ route('bands.index') }}">Band</a></li>
    <li class="breadcrumb-item" aria-current="page">Albums</li>
    <li class="breadcrumb-item active" aria-current="page">{{ $band->name}}</li>
  </ol>
</nav>

<nav class="navbar navbar-light float-right mb-4">

  <form class="form-inline">

    <input name="buscar" class="form-control mr-sm-2" type="search" placeholder="Name" aria-label="Search" autocomplete="off">

    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    <a href="{{route('bands.albums.index', $band)}}" class="btn btn-danger my-2 my-sm-2" type="submit">Clear</a>
  </form>

</nav>

<div class="row">
    @include('custom.message')
</div>

<!-- List albums -->
<table class="table table-hover">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Albums</th>
        <th scope="col">Tracks</th>
        <th scope="col">Year</th>
        <th scope="col">Artist</th>
        <th scope="col"></th>
        <th scope="col"></th>
      </tr>
    </thead>
    <tbody>
     @foreach ($albums as $album)
     <tr>
        <td scope="row">{{ $album->id }}</td>
        <td scope="row"><a href="{{ route('albums.show', $album->id) }}">{{ $album->name }}</a></td>
        <td scope="row"><a href="{{ route('albums.tracks.index', $album) }}">{{ $album->tracks_count }}</a></td>
        <td scope="row">{{ $album->year }}</td>
        <td scope="row"><a href="{{ route('albums.artist_albums.index',$album) }}">{{ $album->rel_artist_album_count }}</a></td>
        <td scope="row"><a href="{{ route('albums.edit', $album) }}" class="btn btn-primary">
        <i class="fa fa-edit"></i>
        </a></td>
        <td scope="row">
        <button class="btn btn-warning btnEliminar" data-id="{{ $album->id }}" data-toggle="modal" data-target="#modalEliminar"><i class="fa fa-trash"></i></button>
          <form action="{{ route('albums.destroy', $album) }}" id="formEli_{{ $album->id}}" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{ $album->id }}">
            <input type="hidden" name="_method" value="delete">
            
          </form>
        </td>
      </tr>
     @endforeach
    </tbody>
  </table>
  <span>
    {{ $albums->links() }}
  </span>

<div class="modal-footer">
  <a href="{{ route('bands.index') }}" class="float-left btn btn-success">Back</a>
</div>
<!-- Modals -->
{{-- @include('albums.modalCreate') --}}
@include('albums.modalDelete')
<!-- EndModals -->

@endsection

@section('scripts')
      <script>

        var idEliminar = 0;

          $(document).ready(function(){
            $(".btnEliminar").click(function(){
                idEliminar = $(this).data('id');
            });
            $(".btnModalEliminar").click(function(){
                $("#formEli_" + idEliminar).submit();
            });

          });
      </script>
  @endsection

