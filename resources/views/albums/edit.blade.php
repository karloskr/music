@extends('layouts.main')

@section('title', 'Edit albums')

@section('content')
<br>

<div class="form-group">
  <form method="POST" action="{{route('albums.update',$album)}}"  enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <h2>Edit Album</h2>
    {{-- Labels List --}}
    <div class="form-group">            
      <div class="form-group">
         <input type="text" id="label_name" class="form-control input-lg" placeholder="Enter Label Name" autocomplete="off" value="{{ $album->labels->name }}"/>
         <div id="albumsList">
         </div>
        @csrf
    <input type="hidden" name="label_id" id="label_id" value="{{ $album->labels->id }}" >
      @error('label_id')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
      @enderror
      <br>
    <br>

    <label for="name">Name</label>
    <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $album->name) }}" autocomplete="off">
    @error('name')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <label for="name">Year</label>
    <input type="number" class="form-control" id="year" name="year" value="{{ old('year', $album->year) }}">
    </div>
    @error('year')
      <p class="help is-danger" style="color: red">{{ $message }}</p>
    @enderror

    <br>
    <div class="form-group">
      <label for="imagenes">Images</label>
      <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
    </div>
    
<br>

    <div class="container" id="text" {{-- style="display:none" --}}>
      <div class="row row-cols-3">
        @foreach ($styles as $style)
          <div class="col"> <input type="checkbox" name="style[]"  id="style_{{ $style->id }}" 
            value="{{ $style->id }}"
            @if (is_array(old('style')) && in_array("$style->id", old('style'))  )
                checked
            @elseif (is_array($album_style) && in_array("$style->id", $album_style)  )
              checked
            @endif>
            {{ $style->name }}</div>
        @endforeach
      </div>
    </div>
  <br>
</div>
    
    <div class="modal-footer">
      <a href="{{ route('bands.albums.index', $band) }}" class="float-right btn btn-success">Back</a>
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
  
</div>
  
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
  
  $('#label_name').keyup(function(){ 
         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.label') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            $('#albumsList').fadeIn();  
            $('#albumsList').html(data);
           }
          });
         }
     });
  
     $(document).on('click', 'li', function(){  
         $('#label_name').val($(this).text());  
         $('#albumsList').fadeOut();  
     });  
  
  });
  </script>
@endsection
