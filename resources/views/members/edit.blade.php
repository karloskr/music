@extends('layouts.main')

@section('title', 'Update Period')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Members</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('members.update',$member)}}">
                    @csrf
                    @method('PUT')

                    <label for="artist_name">Artists</label>
                        {{-- Artists List --}}
                          <div class="form-group">
                            <input type="text" id="artist_name" class="form-control input-lg" placeholder="Enter Artist Name" autocomplete="off" value="{{ $member->relArtist->name }}"/>
                            <div id="artistsList">
                            </div>
                          @csrf
                      <input type="hidden" name="artist_id" id="artist_id" value="{{ $artist->id }}">
                        @error('artist_id')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                        @enderror
                        <br>
                        
                  <label for="init">Start</label>
                  <input type="number" class="form-control" id="init" name="init" value="{{ old('init', $member->init) }}">
                  <br>
                  @error('init')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror

                  <label for="end">End</label>
                  <input type="number" class="form-control" id="end" name="end" value="{{ old('end', $member->end) }}">
                  @error('end')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                  @enderror
                </div>
                    
                  <div class="model-footer">
                    <a type="button" class="btn btn-success" href="{{ route('bands.members.index',$band->id) }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script>
  artistas = [];
  $(document).ready(function(){
  $('#artist_name').keyup(function(){ 

         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.artists') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            artistas = data;
            lista = '<ul class="dropdown-menu" style="display:block; position:relative">';
            data.forEach(function(row){ 
            lista += '<li><a href="#" onclick="cambiarArtist(' + row.id +')">' + row.name + '</a></li>';
            });
            lista += '</ul>';
            $('#artistsList').fadeIn();  
            $('#artistsList').html(lista);
           }
          });
         }
     });
  });

  function cambiarArtist(id) {
    var artista = artistas.find(c => {
      return c.id == id;
    });

    $('#artist_name').val(artista.name);
    $('#artist_id').val(id);
    $('#artistsList').fadeOut();

  }

  
  </script>
@endsection

