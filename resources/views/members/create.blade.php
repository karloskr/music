@extends('layouts.main')

@section('title', 'Create Member')

@section('content')

<div class="card">
    <h5 class="card-header">Create Member</h5>
    <div class="card-body">
        <form method="POST" action="{{route('bands.members.store', $band)}}">
            @csrf
            <div class="form-group">
              <label for="artist_name">Artists</label>
              {{-- Artists List --}}
                <div class="form-group">
                  <input type="text" id="artist_name" class="form-control input-lg" placeholder="Enter Artist Name" autocomplete="off"/>
                  <div id="artistsList">
                  </div>
                @csrf
            <input type="hidden" name="artist_id" id="artist_id">
              @error('artist_id')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <br>
 
              <label for="name">Inicio</label>
              <input type="number" class="form-control" id="init" name="init">
              @error('init')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
              <br>
              <label for="name">End</label>
              <input type="number" class="form-control" id="end" name="end">
              @error('end')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror       

            </div>
          
          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('bands.members.index',$band->id) }}">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
    
@endsection

@section('scripts')
<script>
  $(document).ready(function(){
  
  $('#artist_name').keyup(function(){ 
         var query = $(this).val();
         if(query != '')
         {
          var _token = $('input[name="_token"]').val();
          $.ajax({
           url:"{{ route('search.artists') }}",
           method:"POST",
           data:{query:query, _token:_token},
           success:function(data){
            var lista = '<ul class="dropdown-menu" style="display:block; position:relative">';
            data.forEach(function(row){ 
              lista += '<li><a href="#" onclick="cambiarArtist(' + row.id +', `' + row.name + '`)">' + row.name + '</a></li>';
            });
            lista += '</ul>';
            $('#artistsList').fadeIn();  
            $('#artistsList').html(lista);
           }
          });
         }
     });
  });

  function cambiarArtist(id, name) {
    $('#artist_name').val(name);
    $('#artist_id').val(id);
    $('#artistsList').fadeOut();
    
  }

  </script>
@endsection