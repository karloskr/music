<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="#">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-record-vinyl"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Collection<sup>'</sup></div>
    </a>

    <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
            aria-expanded="true" aria-controls="collapseTwo">
            <i class="fas fa-fw fa-cog"></i>
            <span>Inicio</span>
        </a>

        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div class="bg-white py-8 collapse-inner rounded">
                <h6 class="collapse-header">Components:</h6>
                <a class="collapse-item" href="{{ route('countries.index') }}">Countries</a>
                <a class="collapse-item" href="{{ route('labels.index') }}">Labels</a>
                <a class="collapse-item" href="{{ route('artists.index') }}">Artists</a>
                <a class="collapse-item" href="{{ route('bands.index') }}">Bands</a>
                <a class="collapse-item" href="{{ route('genders.index') }}">Genders</a>
            </div>
        </div>
    </li>
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('info-bands')}}">
        <div class="sidebar-brand-text mx-3">Bands</div>
    </a>
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('info-artists')}}">
        <div class="sidebar-brand-text mx-3">Artists</div>
    </a>
</ul>