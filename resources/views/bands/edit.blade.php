@extends('layouts.main')

@section('title', 'Update Bands')

@section('content')
<br>
  <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h2>Edit Bands</h2>
                </div>
                <div class="card-body">
                  <form method="POST" action="{{route('bands.update', $band)}}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
            
                    <div class="form-group">
                    <label for="exampleFormControlSelect1">List Countries</label>

                    <div class="form-group">
                       <input type="text" id="country_name" class="form-control input-lg" placeholder="Country Name" 
                       autocomplete="off" value="{{$band->country->name }}"/>
                       <div id="countryList">
                       </div>
                      @csrf
                  <input type="hidden" name="country_id" id="country_id" value="{{$band->country->id }}">

                    @error('country_id')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror
                      <label for="name">Name Band</label>
                      <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $band->name )}}">
                      @error('name')
                    <p class="help is-danger" style="color: red">{{ $message }}</p>
                    @enderror
                    </div>

                    <label for="name">Duration</label>
                        {{-- Time --}}
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text">Start</span>
                          </div>
                    
                          <input type="number" class="form-control" placeholder="Insert Start" id="start" name="start" value="{{ old('start', $band->start) }}" min="1900" max="2021">
                    
                          @error('start')
                            <p class="help is-danger" style="color: red">{{ $message }}</p>
                            @enderror
                    
                          <div class="input-group-prepend">
                            <span class="input-group-text">End</span>
                          </div>
                    
                          <input type="number" class="form-control" placeholder="Insert End" id="end" name="end" value="{{ old('end', $band->end) }}" min="1900" max="2021">
                    
                          @error('end')
                          <p class="help is-danger" style="color: red">{{ $message }}</p>
                          @enderror      
                        </div>
                        <br>
                        <label for="history">History</label>
                        <textarea class="form-control" id="history" name="history" value="{{ old('history') }}">{{ $band->history }}</textarea>
                        @error('history')
                          <p class="help is-danger" style="color: red">{{ $message }}</p>
                          @enderror

                    <br>
                          <div class="form-group">
                            <label for="imagenes">Images</label>
                            <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
                          </div>
                          
                    <br>

                    <div class="form-group">
                      <h3>Add Gender</h3>
                          <br>
                          {{-- Genders --}}
                          <div class="input-group">
                            <div class="input-group-prepend">
                              <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
                          </div>
                          <select name="gender_id" class="form-control">
                              <option class="hidden" selected disabled>Genders</option>
                              @foreach ($genders as $gender)
                                <option value="{{ $gender->id }}"
                                  @if ($gender->id === $band->gender_id)
                                    selected
                                  @endif
                                  >
                                  {{ $gender->name }}</option>
                              @endforeach
                          </select>
                      </div>
                      @error('gender_id')
                        <p class="help is-danger" style="color: red">{{ $message }}</p>
                      @enderror
                    
                    <div class="modal-footer">
                      <a type="button" class="btn btn-success" href="{{ route('bands.index') }}">Back</a>
                    <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
       {{-- Libreria JQuery --}}
    

    <script>
$(document).ready(function(){

$('#country_name').keyup(function(){ 
       var query = $(this).val();
       if(query != '')
       {
        var _token = $('input[name="_token"]').val();
        $.ajax({
         url:"{{ route('search.fetch') }}",
         method:"POST",
         data:{query:query, _token:_token},
         success:function(data){
          $('#countryList').fadeIn();  
                   $('#countryList').html(data);
         }
        });
       }
   });

   $(document).on('click', 'li', function(){  
       $('#country_name').val($(this).text());  
       $('#countryList').fadeOut();  
   });  

});

  </script>
@endsection