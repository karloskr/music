@extends('layouts.main')

@section('title', 'Show Band')

@section('content')

<div class="card">
    <h5 class="card-header">Show Band</h5>
    <div class="card-body">
        <form>
            <div class="form-group">
              
                <div class="form-group">
                    <label for="exampleFormControlSelect1">List Countries</label>
                    <select disabled class="form-control" id="countries" name="countries">
                        <option class="hidden" disabled>List Countries</option>
                        @foreach ($countries as $country)
                          <option value="{{ $country->id }}"
                            @if ($country->id === $band->country_id)
                                selected
                            @endif
                            >
                            {{ $country->name }}</option>
                        @endforeach
                    </select>
                </div>

              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name', $band->name) }}" disabled>
            </div>
          
          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('bands.index') }}">Back</a>
          </div>
        </form>
    </div>
  </div>
    
@endsection