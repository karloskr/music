@extends('layouts.main')

@section('title', 'Create Band')

@section('content')

<div class="card">
    <h5 class="card-header">Create Band</h5>
    <div class="card-body">
        <form method="POST" action="{{route('bands.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">

              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" autocomplete="off">
              @error('name')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
            </div>

            <div class="form-group">
              <label for="autocomplete">Select a Country: </label>
               <input type="text" id="country_name" class="form-control input-lg" placeholder="Enter Country Name" autocomplete="off" />
               <div id="countryList">
               </div>
              @csrf
          <input type="hidden" name="country_id" id="country_id">

          <br>
          <label for="name">Duration</label>
          {{-- Time --}}
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Start</span>
            </div>
      
            <input type="number" class="form-control" placeholder="Insert Start" id="start" name="start" value="{{ old('start') }}" min="1900" max="2021">
      
            @error('start')
              <p class="help is-danger" style="color: red">{{ $message }}</p>
              @enderror
      
            <div class="input-group-prepend">
              <span class="input-group-text">End</span>
            </div>
      
            <input type="number" class="form-control" placeholder="Insert End" id="end" name="end" value="{{ old('end') }}" min="1900" max="2021">
      
            @error('end')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror      
          </div>
<br>
          <label for="history">History</label>
          <textarea class="form-control" id="history" name="history" value="{{ old('history') }}"></textarea>
          @error('history')
            <p class="help is-danger" style="color: red">{{ $message }}</p>
            @enderror

      <br>
            <div class="form-group">
              <label for="imagenes">Images</label>
              <input type="file" class="form-control-file" id="imagenes" name="imagenes" accept="img/*">
            </div>
            
      <br>

      <div class="form-group">
        <h3>Add Gender</h3>
            <br>
            {{-- Genders --}}
            <div class="input-group">
              <div class="input-group-prepend">
                <div class="input-group-text"><i class="fa fa-map-marker-alt text-info"></i></div>
            </div>
            <select name="gender_id" class="form-control">
                <option class="hidden" selected disabled>Genders</option>
                @foreach ($genders as $gender)
                  <option value="{{ $gender->id }}">
                    {{ $gender->name }}</option>
                @endforeach
            </select>
        </div>
        @error('gender_id')
          <p class="help is-danger" style="color: red">{{ $message }}</p>
        @enderror

          <div class="modal-footer">
            <a type="button" class="btn btn-success" href="{{ route('bands.index') }}">Back</a>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </form>
    </div>
  </div>
    
@endsection

@section('scripts')
       {{-- Libreria JQuery --}}
    
<script>
$(document).ready(function(){

$('#country_name').keyup(function(){ 
       var query = $(this).val();
       if(query != '')
       {
        var _token = $('input[name="_token"]').val();
        $.ajax({
         url:"{{ route('search.fetch') }}",
         method:"POST",
         data:{query:query, _token:_token},
         success:function(data){
          $('#countryList').fadeIn();  
                   $('#countryList').html(data);
         }
        });
       }
   });

   $(document).on('click', 'li', function(){  
       $('#country_name').val($(this).text());  
       $('#countryList').fadeOut();  
   });  

});
</script>
@endsection