<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GenderController;
use App\Http\Controllers\StyleController;
use App\Http\Controllers\LabelController;
use App\Http\Controllers\AlbumController;
use App\Http\Controllers\BandController;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\TrackController;
use App\Http\Controllers\ArtistController;
use App\Http\Controllers\MemberController;
use App\Http\Controllers\ArtistTrackController;
use App\Http\Controllers\ImagenController;
use App\Http\Controllers\ArtistAlbumController;
use phpDocumentor\Reflection\Types\Boolean;

//HOME


Route::get('/', function () {
    return view('layouts.main');
});

Route::get('/home', function () {
    return view('home');
});

Route::prefix('admin')->group(function () {
    Route::get('/', function () { return view('admin');});
    Route::get('/usuarios', function () { return view('usuarios');});
});

Route::resource('genders', GenderController::class)->names('genders');
Route::resource('genders.styles', StyleController::class)->shallow();
Route::post('/search-styles', [StyleController::class, 'fetch'])->name('search.styles');

Route::resource('labels', LabelController::class)->names('labels');
Route::post('/search-labels', [LabelController::class, 'fetch'])->name('search.label');

Route::resource('countries', CountryController::class)->names('countries');
Route::post('/search-countries', [CountryController::class, 'fetch'])->name('search.fetch');

Route::resource('bands', BandController::class)->names('bands');

/* Vistas */
Route::get('info-bands', [BandController::class,'indexBands'])->name('info-bands');
Route::get('show-bands/{band}', [BandController::class,'showBand'])->name('show-bands');
Route::get('show-album/{album}', [AlbumController::class,'showAlbum'])->name('show-album');

Route::post('/search-bands', [BandController::class, 'fetch'])->name('search.bands');

Route::resource('bands.members', MemberController::class)->shallow();
Route::resource('bands.albums', AlbumController::class)->shallow();


Route::resource('albums.artist_albums', ArtistAlbumController::class)->shallow();

Route::resource('albums.tracks', TrackController::class)->shallow();
Route::resource('tracks.artist_tracks', ArtistTrackController::class)->shallow();

Route::get('info-artists', [ArtistController::class,'indexArtists'])->name('info-artists');
Route::get('show-artists/{artist}', [ArtistController::class,'showArtists'])->name('show-artists');

Route::resource('artists', ArtistController::class)->names('artists');
Route::post('/search-artists', [ArtistController::class, 'fetch'])->name('search.artists');

//Filesystem-disks
Route::get('imagenes/{discs}/{imagen}', ImagenController::class)->name('download_imagen');

/* Route::get('ejemplo', function(){    
})->name('ejemplo'); */
