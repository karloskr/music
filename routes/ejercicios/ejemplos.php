<?php

// 1) convertir un numero en negativo

$num = -0.12;
$num = $num <= 0 ? $num : -$num; //operador ternario

// 2) Verifique si una cadena tiene la misma cantidad de 'x' y 'o'. El método debe devolver un valor booleano y no distinguir entre mayúsculas y minúsculas. La cadena puede contener cualquier carácter.

$xo = 'xX+oO';

$lower = strtolower($xo);
if(substr_count($lower,'x') == substr_count($lower,'o')){
    echo 'true'; 
}else{
    echo 'false'; 
}

// 3)  dada una cadena, reemplace cada letra con su posición en el alfabeto. Si algo en el texto no es una letra, ignórelo y no lo devuelva. "a" = 1, "b" = 2etc.


/* function alphabet_position($string){

$low = strtolower($string);
$res = '';

    for ($i=0; $i <strlen($string); $i++) { 

        if($low[$i] >= 'a' && $low[$i] <= 'z'){
            $res .= (ord($low[$i]) - ord('a'))+1 ." ";
        }
    }
    return trim($res);
} */

echo alphabet_position('The sunset sets at twelve o\' clock.');

function alphabet_position(string $s): string {
    $abc = array_flip(range('a', 'z'));
  
      $string = [];
      foreach(str_split(strtolower($s)) as $letter) {
          if(isset($abc[$letter])) {
              $string[] = $abc[$letter] + 1;
          }
      }
  
      return implode(' ', $string);
  }
  echo alphabet_position('The sunset sets at twelve o\' clock.');


  //4 Dada una matriz de números enteros, encuentre el que aparece un número impar de veces. Siempre habrá un solo número entero que aparezca un número impar de veces.


  function findIt(array $seq)
{
    $impar = 0;
    for ($i=0; $i < count($seq); $i++) { 

        $val = $seq[$i] %2== !$impar;
        
       
    }
  
}

echo findIt([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]);

$i = 1;     // contar los números generados
$n = 100;   // cuantos impares se deben generar
$impar = 1; // el numero impar generado
while ( $i <= $n){
    echo "$impar <br />";
    $i = $i + 1;
    $impar = $impar + 2;
}


/* 5) Se le dará una matriz de números enteros. Su trabajo es tomar esa matriz y encontrar un índice N donde la suma de los enteros a la izquierda de N es igual a la suma de los enteros a la derecha de N. Si no hay un índice que haga que esto suceda, regrese -1. */

/* function find_even_index($arr){
    $sum = 0;
    $leftSum = 0;

     for ($i=0; $i < count($arr); $i++) { 
         
         $suma = $sum += $arr[$i];
         var_dump($suma);
     }
     echo '<br>';
     for ($i=0; $i < count($arr); $i++) { 
         
         $suma = $sum -= $arr[$i];
         var_dump($suma);

         if ($leftSum === $sum) {
             return $i;
           }

           $leftSum += $arr[$i];
     }

     return -1;
   }
 
 echo find_even_index(array(1,2,3,4,5,3,2,1)); */

 function find_even_index($arr){
    /* $resultIndex = 0; */
  
      for($index = 0; $index < count($arr); $index++){
          $arrLeft = array_slice($arr, 0, $index);
          $arrRight = array_slice($arr, $index+1);
  
          $arrLeftSum = array_sum($arrLeft);
          $arrRightSum = array_sum($arrRight);
  
          if($arrLeftSum == $arrRightSum){
              return $index;
          }
      }
  
      return -1;
  }
  echo find_even_index(array(1,100,50,-51,1,1));

  /* 6) Dada una cadena de dígitos, debe reemplazar cualquier dígito por debajo de 5 con '0' y cualquier dígito de 5 y superior con '1'. Devuelve la cadena resultante */

  function fake_bin($s){
    
    $arr1 = str_split($s);
    $string = '';
    for ($i=0; $i < count($arr1); $i++) { 
        /* echo $arr1[$i]; */
        if($arr1[$i] >= 5){
            $string .= 1;
        }else{
            $string .= 0;
        }

    }
    return $string;
    /* foreach ($arr1 as $value) {
        if($value >= 5){
            echo 1;
        }
        if($value < 5){
            echo 0;
        }
        
    } */
    
  }

  echo fake_bin('45385593107843568');


 /* 7) Escribe una función, persistenceque toma un parámetro positivo numy devuelve su persistencia multiplicativa, que es la cantidad de veces que debes multiplicar los dígitos numhasta llegar a un solo dígito. */

 function persistence($num){
    $count = 0;
    while ($num > 9) {
      $num = array_product(str_split($num));
      /* var_dump($num); */
      $count++;
    }

    return $count;
  }
  
echo persistence(39);

/* 8) Se te va a dar una palabra. Su trabajo es devolver el carácter intermedio de la palabra. Si la longitud de la palabra es impar, devuelve el carácter del medio. Si la longitud de la palabra es par, devuelve los 2 caracteres del medio. */
function getMiddle($text) {
    $len = strlen($text);
    $half = $len / 2; 
    if($len%2 == 0){
        return substr($text, (floor($half) - 1), 2);
    }else{
        return substr($text, $half, 1);
    }
   
}
echo getMiddle("test");

/* function getMiddle($text) {
    $len = strlen($text);

    if($len % 2 == 0) {
        return substr($text, $len/2-1, 2);
    
    } else {
        return substr($text, $len/2, 1);
    }
   
}
echo getMiddle("testingf"); */


/* 9) Dada una serie de palabras, debe encontrar la palabra con la puntuación más alta.
    Cada letra de una palabra puntúa según su posición en el alfabeto: a = 1, b = 2, c = 3etc.
    Debe devolver la palabra con la puntuación más alta como una cadena.
    Si dos palabras obtienen la misma puntuación, devuelva la palabra que aparece antes en la cadena original.
    Todas las letras serán minúsculas y todas las entradas serán válidas. */

    function high($x) {
        $words = explode(" ", $x);
        $bestWord = "";
        $bestScore = 0;
        
        foreach ($words as $word) {
          $score = 0;
          $letters = str_split($word);

          foreach ($letters as $letter) {
            $score += ord($letter) - 96;
            var_dump($score);
          }
          
          if ($score > $bestScore) {
              var_dump($score);
            $bestScore = $score;
            $bestWord = $word;
          }
        }
        
        return $bestWord;
      }

      echo high('man i need a taxi up to ubud');

      /* Ejemplo array_map */
      /* $array_map = array( 10,20,30 );

        function multiple_array_values($n){
        return $n * 2;
        }
        
        $new_array = array_map('multiple_array_values', $array_map );
        var_dump($new_array); */

    /* 10) Dada una lista de números enteros, determina si la suma de sus elementos es par o impar.
    Da tu respuesta como una cadena de coincidencia "odd"o "even".
    Si la matriz de entrada está vacía, considérelo como: [0](matriz con cero). */

    /* function odd_or_even(array $a){
        $suma = array_sum($a);
        $impar = $suma%2;
    
        if ( $impar == 0 ) {
        return 'even';
        }else{
        return 'odd'; 
        }
    
        return $suma;
    }
    odd_or_even([2, 5, 34, 6]); */

    function odd_or_even(array $a): string {
        return array_sum($a) % 2 ? 'odd' : 'even';
      }

    /* 11) Mi abuelo siempre predijo cómo envejecerían las personas y, justo antes de morir, ¡reveló su secreto!
    ¡En honor a la memoria de mi abuelo, escribiremos una función usando su fórmula!
    Haz una lista de las edades en las que murió cada uno de tus bisabuelos.
    Multiplica cada número por sí mismo.
    Súmalos todos juntos.
    Saca la raíz cuadrada del resultado.
    Dividir por dos. */

    function predictAge($age1,$age2,$age3,$age4,$age5,$age6,$age7,$age8){

        $union = $age1.$age2.$age3.$age4.$age5.$age6.$age7.$age8;
        $split = str_split($union, 2);
        $suma = 0;
    
        foreach ($split  as $producto) {
        $mult = $producto *= $producto;
        $suma += $mult;
        }
        $raiz = ceil(sqrt($suma)/2)-1;
        echo $raiz;
    
    /*  return 0; */ // do it
    }
    
    echo predictAge(65,60,75,55,60,63,64,45);//86
    
    /* function predictAge($age1,$age2,$age3,$age4,$age5,$age6,$age7,$age8){
        
        $arr = [$age1, $age2, $age3, $age4, $age5, $age6, $age7, $age8];
        
        for($i=0;$i<count($arr);$i++) {
        $arr[$i] *= $arr[$i];
        }
        
        $predict = sqrt(array_sum($arr)) / 2;
        
        return floor($predict);
        
    } */

    /* 
12) Complete la solución para que devuelva verdadero si el primer argumento (cadena) pasado termina con el segundo argumento (también una cadena). 
strEndsWith('abc', 'bc') -- returns true
strEndsWith('abc', 'd') -- returns false
*/

function solution($str, $ending) {

    $endln = strlen($ending);
    var_dump($endln);
    if($endln===0)
    return true;
    $sbstr = substr($str,-$endln);
    var_dump($sbstr);
    return $ending === $sbstr ? 'true' : 'false';
    
  }

  /* echo solution("samurai", "ai").'<br>'; //true */
    /* echo solution("ninja", "do").'<br>'; //false */ 

    

    

?>