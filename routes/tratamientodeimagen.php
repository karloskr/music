<?php

$band = App\Models\Band::find(1);

$image = $band->image;
if($image){
    echo 'tiene una imagen';
}
else{
    echo 'no tiene una imagen';
}

/* 01 Crear una imagen para un usuario utilizando save() */
$imagen = new App\Models\Image(['url' => 'img/ossian.jpg']);
    
    $band = App\Models\Band::find(1);

    $band->image()->save($imagen);

return $band;

/* 02 obtenerr la informacion de la imagen a traves de la banda*/

$band = App\Models\Band::find(1);

return $band->image->url;


/* 03 Crear varias imagenes para los albums utilizando el metodo saveMany */
$albums = App\Models\Album::find(3);

$albums->images()->saveMany([
    new App\Models\Image(['url' => 'img/helstar.jpg']),
    new App\Models\Image(['url' => 'img/ossian.jpg']),
    new App\Models\Image(['url' => 'img/omen.jpg']),
]);

return $albums;

/* 04 Crear una imagen para un usuario utilizando create() */
    
$band = App\Models\Band::find(1);

$band->image()->create([
    'url' => 'img/ossian.jpg',
]);

return $band;

/* 04 Otra manera */
    
$imagen = [];

$imagen['url'] = 'img/ossian.jpg';

$band = \App\Models\Band::find(1);

$band->image()->create($imagen);

return $band;

/* 05 Crear varias imagenes para unos albums utilizando el metodo createMany */
    
$imagen = [];

$imagen[]['url'] = 'img/H-lords.jpg';
$imagen[]['url'] = 'img/helstar.jpg';
$imagen[]['url'] = 'img/ossian.jpg';
$imagen[]['url'] = 'img/axe_victims.jpg';

$albums = \App\Models\Album::find(2);

$albums->images()->createMany($imagen);

return $albums->images;


/* 06 Actualizar una imagen de bandas */
    
$band = \App\Models\Band::find(1);
$band->image->url='img/H-lords.jpg';
$band->push();
return $band->image;

/* 07 Actualizar una imagen de albums */
    
$albums = \App\Models\Album::find(3);
    
$albums->images[0]->url='img/axe_victims.jpg';

$albums->push(); 

return $albums->images;

/* 08 Buscar registro relacionado con la imagen */
    
$image = App\Models\Image::find(15);

return $image->imageable;


/* 09 Delimitar los registros */
    
$albums = \App\Models\Album::find(3);

return $albums->images()->where('url','img/axe_victims.jpg')->get();


/* 11 Contar los registros relacionados albums*/
    
$band = \App\Models\Band::withCount('image')->get();
$band = $band->find(1);
return $band;

/* 12 Contar los registros relacionados albums*/
    
$albums = \App\Models\Album::withCount('images')->get();
$albums = $albums->find(3);
return $albums->images_count;

/* 13 Contar registros relacionados a albums con loadCount*/
    
$albums = \App\Models\Album::find(2);

return $albums->loadCount('images');

/* 15 Carga previa album(eager loading)*/
    
$albums = \App\Models\Album::with('images')->get();

return $albums;

/* 16 Carga previa band(eager loading)*/
    
$band = \App\Models\Band::with('image')->get();

return $band;

/* 17 Carga previa de multiples relaciones*/
    
$band = \App\Models\Band::with('image')->get();

return $band->image->url;

/* 18 Carga previa de multiples relaciones de una banda especifica*/
    
$band = \App\Models\Band::with('image')->find(2);

return $band->image->url;

/* 19 Delimitar los campos*/
    
$band = \App\Models\Band::with('image:id,imageable_id,url','albums:id,band_id,name')->find(1);

return $band;


/* 20 eliminar una imagen*/
    
$albums = \App\Models\Album::find(2);
$albums->images[0]->delete();
return $albums;


/* 21 eliminar todas las imagenes*/
    
$albums = \App\Models\Album::find(2);
$albums->images()->delete();
return $albums;


/* subir varias imagenes */

$urlimagenes = [];

        if($request->hasFile('imagenes')){

            $imagenes = $request->file('imagenes');

            foreach($imagenes as $imagen){

                $nombre = time().'_'.$imagen->getClientOriginalName();

                $ruta = public_path().'/img';

                $imagen->move($ruta,$nombre);

                $urlimagenes[]['url'] = '/img/'.$nombre;

            }
            return $urlimagenes;
        }

        
/* 
        
Route::get('/prueba', function () {
    
    $imagen = [];

    $imagen[]['url'] = 'img/H-lords.jpg';
    $imagen[]['url'] = 'img/helstar.jpg';
    $imagen[]['url'] = 'img/ossian.jpg';
    $imagen[]['url'] = 'img/axe_victims.jpg';
    
    $albums = \App\Models\Album::find(11);
    
    $albums->images()->createMany($imagen);
    
    return $albums->images;

});

Route::get('/resultados', function () {
    $image = Image::orderBy('id', 'desc')->get();
    return $image;
}); */