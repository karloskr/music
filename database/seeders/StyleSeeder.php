<?php

namespace Database\Seeders;

use App\Models\Gender;
use App\Models\Style;
use Illuminate\Database\Seeder;

class StyleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /* Style::factory(10)->create(); */

       $styles = [

        /* Blues */
        ['name' => 'Acid Rock',            'gender_id' => 13], 
        ['name' => 'Acoustic',             'gender_id' => 13],
        ['name' => 'Alternative Rock',     'gender_id' => 13],
        ['name' => 'AOR',                  'gender_id' => 13],
        ['name' => 'Arena Rock',           'gender_id' => 13],
        ['name' => 'Atmospheric Black Metal','gender_id' => 13],
        ['name' => 'Black Metal',          'gender_id' => 13],
        ['name' => 'Blues Rock',      'gender_id' => 13],
        ['name' => 'Classic Rock','gender_id' => 13],
        ['name' => 'Coldwave',          'gender_id' => 13],
        ['name' => 'Country Rock',       'gender_id' => 13],
        ['name' => 'Death Metal',          'gender_id' => 13],
        ['name' => 'Deathcore',          'gender_id' => 13],
        ['name' => 'Deathrock',          'gender_id' => 13],
        ['name' => 'Depressive Black Metal',          'gender_id' => 13],
        ['name' => 'Doom Metal',          'gender_id' => 13],
        ['name' => 'Folk Metal',          'gender_id' => 13],
        ['name' => 'Folk Rock',          'gender_id' => 13],
        ['name' => 'Funeral Doom Metal',          'gender_id' => 13],
        ['name' => 'Glam',          'gender_id' => 13],
        ['name' => 'Hard Rock',          'gender_id' => 13],
        ['name' => 'Heavy Metal',          'gender_id' => 13],
        ['name' => 'Melodic Death Metal',          'gender_id' => 13],
        ['name' => 'New Wave',          'gender_id' => 13],
        ['name' => 'Power Metal',          'gender_id' => 13],
        ['name' => 'Prog Rock',          'gender_id' => 13],
        ['name' => 'Progressive Metal',          'gender_id' => 13],
        ['name' => 'Rock & Roll',          'gender_id' => 13],
        ['name' => 'Symphonic Metal',          'gender_id' => 13],
        ['name' => 'Thrash',          'gender_id' => 13],
        ['name' => 'Viking Metal',          'gender_id' => 13],

           /* Rock */
        ['name' => 'Boogie Woogie',        'gender_id' => 14], 
        ['name' => 'Chicago Blues',        'gender_id' => 14],
        ['name' => 'Country Blues',        'gender_id' => 14],
        ['name' => 'Delta Blues',          'gender_id' => 14],
        ['name' => 'East Coast Blues',     'gender_id' => 14],
        ['name' => 'Electric Blues',       'gender_id' => 14],
        ['name' => 'Harmonica Blues',      'gender_id' => 14],
        ['name' => 'Hill Country Blues',   'gender_id' => 14],
        ['name' => 'Jump Blues',           'gender_id' => 14],
        ['name' => 'Funk / Soul',          'gender_id' => 14],
        ['name' => 'Louisiana Blues',      'gender_id' => 14],
        ['name' => 'Memphis Blues',        'gender_id' => 14],
        ['name' => 'Modern Electric Blues','gender_id' => 14],
        ['name' => 'Piano Blues',          'gender_id' => 14],
        ['name' => 'Piedmont Blues',       'gender_id' => 14],
        ['name' => 'Texas Blues',          'gender_id' => 14],
        /* Brass & Military */
        ['name' => 'Brass Band',           'gender_id' => 15], 
        ['name' => 'Marches',              'gender_id' => 15],
        ['name' => 'Military',             'gender_id' => 15],
        ['name' => 'Pipe & Drum',          'gender_id' => 15],
    ];

        foreach ($styles as $style) {
            Style::create($style);
        }
    }
}








