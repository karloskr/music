<?php

namespace Database\Factories;

use App\Models\Gender;
use Illuminate\Database\Eloquent\Factories\Factory;

class GenderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Gender::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->randomElement([
                'Electronic', 
                'Reggae',
                'Non-Music',
                'Children',
                'Hip Hop',
                'Latin',
                'Pop',
                'Folk, World, & Country',
                'Jazz',
                'Funk / Soul',
                'Classical',
                'Stage & Screen',
                'Rock',
                'Blues',
                'Brass & Military'
                ]),
            
        ];
    }
}
