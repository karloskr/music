<?php

namespace Database\Factories;

use App\Models\Band;
use App\Models\Country;
use Illuminate\Database\Eloquent\Factories\Factory;

class BandFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Band::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'country_id' => Country::InRandomOrder()->first()->id,
            'name' => $this->faker->name,
            'start' => $this->faker->year,
            'end' => $this->faker->year,
            'history' => $this->faker->text(500),
        ];
    }
}
