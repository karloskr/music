<?php

namespace Database\Factories;

use App\Models\Member;
use App\Models\Artist;
use App\Models\Band;
use Illuminate\Database\Eloquent\Factories\Factory;

class MemberFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Member::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'artist_id' => Artist::inRandomOrder()->first()->id,
            'band_id' => Band::inRandomOrder()->first()->id,
            'init' => $this->faker->year,
            'end' => $this->faker->year,
        ];
    }
}
