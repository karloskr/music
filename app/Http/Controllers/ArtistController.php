<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\Band;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $artists = Artist::where('name', 'like', "%$buscar%")
        ->withCount('albums')
        ->with('country')
        ->orderBy('id','desc')->paginate(5);

        return view('artists.index',compact('artists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        return view('artists.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'country_id' =>   'required|integer',
            'name' =>   'required|max:100|unique:artists,name',
            'profile' =>   'required|max:2000|',
            'day_birth' =>   'nullable|date|',
            'day_death' =>   'nullable|date|',
            'imagenes' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'
        ]);

        $data = $request->all();
        
        if ($request->hasFile('imagenes') && $request->file('imagenes')->isValid()) {

            $data['imagen'] = $request->imagenes->store('','artists');
        }
        /* dd($request->all()); */
        Artist::create($data);

        return redirect()->route('artists.index')->with('artist_store', 'Artist Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function show(Artist $artist)
    { 
        $countries = Country::all();
        return view('artists.show', compact('artist','countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function edit(Artist $artist)
    {
        $countries = Country::all();

        return view('artists.edit', compact('artist', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Artist $artist)
    {
        $request->validate([
            'country_id' =>  'required|integer',
            'name' =>   'required|max:100|unique:artists,name,' .$artist->id,
            'profile' =>   'required|max:2000|',
            'day_birth' =>   'nullable|date|',
            'day_death' =>   'nullable|date|',
            'imagenes' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048'

        ]);

        $data = $request->all();
        unset($data['imagenes']);
        
        if ($request->hasFile('imagenes') && $request->file('imagenes')->isValid()) {
            Storage::disk('bands')->delete($artist->imagen);
            $data['imagen'] = $request->imagenes->store('','artists');
        }

        
        $artist->update($data);
        return redirect()->route('artists.index', $artist)->with('artist_update', 'Artist Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Artist  $artist
     * @return \Illuminate\Http\Response
     */
    public function destroy(Artist $artist)
    {
        /* if($artist->country()->count()){
            return  redirect()->back()->with('artist_destroy', 'Record associated with foreign key');
        } */
        $artist->delete();
        return back()->with('artist_destroy','Artists Deleted');
    }

    public function fetch(Request $request)
    {
        if($request->get('query'))
        {
        $query = $request->get('query');
        return Artist::where('name', 'LIKE', "%$query%")->take(10)->get();
        }
    }

    public function indexArtists(Request $request)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $artists = Artist::where('name', 'like', "%$buscar%")
        ->withCount('bands')
        ->with('country', 'bands')
        ->orderBy('id', 'desc')->paginate(5);

        /* return $artists; */

        return view('sidebar.artists.index', compact('artists'));
    }
    
    public function showArtists(Request $request, Artist $artist)
    {
        $bands = Band::whereHas('albums', function ( $query) use ($artist){ 

            $query->whereHas('relArtistAlbum', function ( $query) use ($artist){
                $query->where('artist_id', $artist->id);
            });
        })->with(['albums' => function ( $query) use ($artist){ 

            $query->whereHas('relArtistAlbum', function ( $query) use ($artist){
                $query->where('artist_id', $artist->id);
            });
        }])->orderBy('id','asc')->paginate(5);
        /* return $bands; */
           
        return view('sidebar.artists.show',compact('artist','bands'));
    }
}
