<?php

namespace App\Http\Controllers;

use App\Models\Style;
use App\Models\Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class StyleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Gender $gender)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $styles = $gender->styles()->where('name', 'like', "%$buscar%")->orderBy('id','desc')->paginate(5);

        return view('styles.index',compact('styles','gender'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Gender $gender)
    {
       return view('styles.create',compact('gender'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Gender $gender)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:styles,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }

        $styles = $gender->styles()->create($request->all());

        return redirect()->route('genders.styles.index',compact('styles','gender'))
        ->with('style_store','Style Created');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Style  $style
     * @return \Illuminate\Http\Response
     */
    public function show(Style $style)
    {   
        $gender = $style->gender;
        return view('styles.show',compact('style', 'gender'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Style  $style
     * @return \Illuminate\Http\Response
     */
    public function edit(Style $style)
    {
        $gender = $style->gender;
        return view('styles.edit',compact('style','gender'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Style  $style
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Style $style)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:50|unique:styles,name',
        ]);

        if($validator->fails()){
            return back()
            ->withErrors($validator)
            ->with('ErrorInsert', 'Field Required')
            ->withInput();
        }

        $style->update($request->all());

        $gender = $style->gender;

        return redirect()->route('genders.styles.index', $gender)->with('style_update', 'Style updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Style  $style
     * @return \Illuminate\Http\Response
     */
    public function destroy(Style $style)
    {
       $style->delete();
       return redirect()->back()->with('style_delete','Style Deleted');
    }

    public function fetch(Request $request)
    {
     if($request->get('query'))
     {
      $query = $request->get('query');
      $data = Style::where('name', 'LIKE', "%$query%")->take(10)->get();
      
      $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
      foreach($data as $row)
      {
       $output .= '
       <li><a href="#" onclick="getElementById(\'style_id\').value = '.$row->id.'">'.$row->name.'</a></li>    
       ';
      }
      $output .= '</ul>';
      echo $output;
     }
    }
}
