<?php

namespace App\Http\Controllers;

use App\Models\AlbumStyle;
use App\Models\Band;
use App\Models\Country;
use App\Models\Gender;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;


class BandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $bands = Band::where('name', 'like', "%$buscar%")
        ->with('country','gender')
        ->withCount('members','albums')
        ->orderBy('id', 'Desc')->paginate(5);
        
        
        return view('bands.index', compact('bands'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Band $band)
    {   
        $countries = Country::all();
        $genders = Gender::all();
        return view('bands.create',compact('countries','genders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'country_id' =>    'required|integer',
            'name' =>   'required|max:50',
            'start' =>   'required|integer|min:1900|max:'.(date('Y')+1),
            'end' =>   'required|integer|min:1900|max:'.(date('Y')+1),
            'history' =>   'nullable|max:4000',
            'imagenes' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048',
            'gender_id' =>    'required|integer',
        ]);
        
        $data = $request->all();
        
        if ($request->hasFile('imagenes') && $request->file('imagenes')->isValid()) {

            $data['imagen'] = $request->imagenes->store('','bands');
        }

        Band::create($data);   
        
        return redirect()->route('bands.index')->with('band_store', 'Bands Created');;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function show(Band $band)
    {   
        $countries = Country::all();

        return view('bands.show', compact('band', 'countries'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function edit(Band $band)
    {
        $countries = Country::all();
        $genders = Gender::all();
        return view('bands.edit',compact('band','countries','genders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Band $band)
    {
        $request->validate([
            'country_id' =>  'required',
            'name' =>   'required|max:50',
            'start' =>   'required|integer|min:1900|max:'.(date('Y')+1),
            'end' =>   'required|integer|min:1900|max:'.(date('Y')+1),
            'history' =>   'nullable|max:4000',
            'imagenes' => 'nullable|image|mimes:jpeg,png,jpg,svg|max:2048',
            'gender_id' =>    'required|integer',
        ]);

        $data = $request->all();
        unset($data['imagenes']);
        
        if ($request->hasFile('imagenes') && $request->file('imagenes')->isValid()) {
            Storage::disk('bands')->delete($band->imagen);
            $data['imagen'] = $request->imagenes->store('','bands');
        }

        $band->update($data);

        return redirect()->route('bands.index', $band)->with('band_update', 'Band updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Band  $band
     * @return \Illuminate\Http\Response
     */
    public function destroy(Band $band)
    {
        if($band->albums()->count() | $band->members()->count()){
            return back()->with('band_destroy', 'Record associated with foreign key');
        }
        $band->delete();
        return back()->with('band_destroy','Band Deleted');

        
    }

    public function fetch(Request $request)
    {
        if($request->get('query'))
        {
            $query = $request->get('query');
            $data = Band::where('name', 'LIKE', "%$query%")->take(10)->get();
            
            $output = '<ul class="dropdown-menu" style="display:block; position:relative">';
            foreach($data as $row)
                {
                    $output .= '
                    <li><a href="#" onclick="getElementById(\'band_id\').value = '.$row->id.'; getElementById(\'band_name\').value = '.$row->name.'">'.$row->name.'</a></li>    
                    ';
                }
            $output .= '</ul>';
            echo $output;
        }

     
    }

    public function indexBands(Request $request)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $bands = Band::whereHas('albums', function ( $query) use ($buscar){ 
                $query->where('name', 'like', "%$buscar%");
                $query->orWhere('year', 'like', "%$buscar%");
        })->with(['albums' => function ( $query) use ($buscar){ 
                $query->where('name', 'like', "%$buscar%");
                $query->orWhere('year', 'like', "%$buscar%");
        }])->with('country')->orderBy('id','asc')->paginate(5);


        return view('sidebar.bands.index', compact('bands'));
    }

    public function showBand(Band $band)
    {
        $albums = $band->albums()->with('styles','labels','images')->orderBy('year')->paginate(5);

        $members = $band->members()->whereHas('relArtist', function ( $query) use ($band){ 
            $query->where('band_id',$band->id);
        })->with(['relArtist.bands'])->orderBy('end')->get();
        
        return view('sidebar.bands.show', compact('band', 'albums','members'));
    }

    
    
}
