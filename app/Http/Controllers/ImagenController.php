<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;

class ImagenController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($discs,$imagen)
    {
        return Storage::disk($discs)->download($imagen);
    }
}
