<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistTrack;
use App\Models\Participation;
use App\Models\Track;
use Illuminate\Http\Request;

class ParticipationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Track $track)
    {
        $artistTrack = $track->relArtistsTracks()->with('relArtist', 'relParticipation')
        ->orderBy('id', 'Desc')->paginate(5);

        $album = $track->albums;
        
        return view('participations.index', compact('artistTrack', 'track','album'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Track $track)
    {
        $artists = Artist::all();
        $participations = Participation::all();
        
        return view('participations.create',compact('track','artists', 'participations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Track $track)
    {   
        /* $request->validate([
        'artist_id' =>  'required|integer',
        'participation_id' =>  'required|integer',
        'name' =>   'required|max:50',
    ]); */

        $track->relArtistsTracks()->create($request->all());
        return redirect()->route('tracks.participations.index', compact( 'track'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {   

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function edit(Track $track)
    {
        $participations = Participation::all();
        $artists = Artist::all();
        $participation = $track->relArtistsTracks;
        return view('participations.edit', compact('track','participations', 'artists','participation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtistTrack $artistTrack)
    {   
        $artistTrack->update($request->all());

        return redirect()>route('tracks.participations.index', compact('artistTrack'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Participation  $participation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Participation $participation)
    {


    }
}
