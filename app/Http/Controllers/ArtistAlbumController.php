<?php

namespace App\Http\Controllers;

use App\Models\ArtistAlbum;
use App\Models\Album;
use App\Models\Participation;
use Illuminate\Http\Request;

class ArtistAlbumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Album $album)
    {
        $artistasyAlbums = $album->relArtistAlbum()->with('relArtist', 'relAlbum')
            ->orderBy('id', 'desc')->paginate(5);

        $band = $album->bands;


        return view('artist_albums.index', compact('artistasyAlbums','album', 'band'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Album $album)
    {
        $artistasyAlbum = $album->relArtistAlbum;
        $band = $album->bands;
        $participations = Participation::all();
        return view('artist_albums.create', compact('album','artistasyAlbum','band','participations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Album $album)
    {
        $request->validate([
            'artist_id' => 'required',
            'participation_id' => 'required',
        ]);
       
        $album->relArtistAlbum()->create($request->all());
        return redirect()->route('albums.artist_albums.store', compact('album'))->with('artistAlbum_store','Artist Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtistAlbum  $artistAlbum
     * @return \Illuminate\Http\Response
     */
    public function show(ArtistAlbum $artistAlbum)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtistAlbum  $artistAlbum
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtistAlbum $artistAlbum)
    {
        $artistasyAlbum = $artistAlbum->relArtist;
        $album = $artistAlbum->relAlbum;
        $participations = Participation::all();

        return view('artist_albums.edit', compact('artistAlbum','artistasyAlbum','album','participations'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtistAlbum  $artistAlbum
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtistAlbum $artistAlbum)
    {
        $request->validate([
            'artist_id' => 'required',
            'participation_id' => 'required',
        ]);
        
        $artistAlbum->update($request->all());
        $album =$artistAlbum->relAlbum;
        return redirect()->route('albums.artist_albums.index',compact('artistAlbum','album'))->with('artistAlbum_update','Artist Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtistAlbum  $artistAlbum
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtistAlbum $artistAlbum)
    {
        $artistAlbum->delete();
        return redirect()->back()->with('artistAlbum_destroy','Artist Deleted');;
    }
}
