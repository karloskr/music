<?php

namespace App\Http\Controllers;

use App\Models\Artist;
use App\Models\ArtistTrack;
use App\Models\Participation;
use App\Models\Track;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;   


class ArtistTrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Track $track)
    {
        $buscar = $request->get('buscar');

        $artist_tracks = $track->relArtistsTracks()->whereHas('relArtist', function (Builder $query) use($buscar){  
        $query->where('name', 'like',"%$buscar%" );
        })->with('relArtist', 'relParticipation')
        ->orderBy('id', 'desc')->paginate(5);
    
        $album = $track->albums;
        return view('participations.index', compact('artist_tracks','track','album'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Track $track)
    {
        $participations = Participation::all();
        
        $artists = Artist::all();
        
        return view('participations.create', compact('track', 'participations', 'artists'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Track $track)
    {   
        $request->validate([
            'artist_id' => 'required',
            'participation_id' => 'required',
        ]);

        $track->relArtistsTracks()->create($request->all());

        return redirect()->route('tracks.artist_tracks.store', $track)
        ->with('participation_store','Participation Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtistTrack  $artistTrack
     * @return \Illuminate\Http\Response
     */
    public function show(ArtistTrack $artist_track)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtistTrack  $artistTrack
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtistTrack $artist_track)
    {
        $participations = Participation::all();
        $track = $artist_track->relTrack;
        $artists = $artist_track->relArtist;

        return view('participations.edit',compact('participations','artist_track','track', 'artists'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtistTrack  $artistTrack
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtistTrack $artist_track )
    {
        $request->validate([
            'artist_id' => 'required',
            'participation_id' => 'required',
        ]);
        
        $artist_track->update($request->all());
        $track = $artist_track->relTrack;
        return redirect()->route('tracks.artist_tracks.index',compact('track','artist_track'))
        ->with('participation_update','Participation Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtistTrack  $artistTrack
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtistTrack $artist_track)
    {
        $artist_track->delete();

        return back()->with('participation_destroy','Participation Deleted');
    }
}
