<?php

namespace App\Http\Controllers;

use App\Models\Album;
use App\Models\Style;
use App\Models\Track;
use Illuminate\Http\Request;

class TrackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,Album $album)
    {
        //Filtrado
        $buscar = $request->get('buscar');

        $tracks = $album->tracks()->where('name', 'like', "%$buscar%")
        ->withCount('relArtistsTracks')->orderBy('id', 'desc')->paginate(5);
 
        $band = $album->bands;
        /* return $tracks; */
        return view('tracks.index ', compact('tracks','album', 'band'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Album $album)
    {
        $styles = Style::all();

        return view('tracks.create', compact('styles','album'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Album $album)
    {
        $request->validate([
            'style_id' =>  'required|integer',
            'name' =>   'required|max:50',
            'hora' => 'nullable|integer|max:2',
            'minutos' => 'nullable|integer|max:60',
            'segundos' => 'nullable|integer|max:60',
        ]);

        $input = $this->calcularTiempo($request);
        
        $album->tracks()->create($input);

        return redirect()->route('albums.tracks.index',compact('album'))->with('track_store','Track Created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function show(Track $track)
    {
        $style = $track->style;
        $album = $track->albums;   
        return view('tracks.show', compact('style', 'track', 'album'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request,Track $track)
    {   
        $styles = Style::all();
        $album = $track->albums;
        
        $duracion = $track->duration;//devuelve la suma de la operacion del store
        
        $hora = intdiv( $duracion, 3600);   //Devuelve el cociente entero de la división de dividend entre divisor.
        $modHora = $duracion % 3600;//operacion modulo
        $minutos = intdiv($modHora, 60);
        $segundos = $duracion % 60;//operacion modulo

        $hours = $track->hora = $hora;
        $minutes = $track->minutos = $minutos;
        $seconds = $track->segundos = $segundos;

        return view('tracks.edit', compact('track','styles', 'album', 'hours', 'minutes', 'seconds'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Track $track)
    {
        $request->validate([
            'style_id' =>  'required|integer',
            'name' =>   'required|max:50',
            'hora' => 'nullable|integer|max:2',
            'minutos' => 'nullable|integer|max:60',
            'segundos' => 'nullable|integer|max:60',
        ]);

        $input = $this->calcularTiempo($request);

        $track->update($input);
        $album = $track->albums;
       
        return redirect()->route('albums.tracks.index', compact('track', 'album'))->with('track_update','Track Updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Track  $track
     * @return \Illuminate\Http\Response
     */
    public function destroy(Track $track)
    {
        /* if($track->albums()->count()){
            return redirect()->back()->with('track_destroy','Record associated with foreign key');  
        } */
        $track->delete();
        return redirect()->back()->with('track_destroy','Track Deleted');
    }

    public function calcularTiempo($request){
        
        $duration = $request->hora * 3600 + $request->minutos * 60 + $request->segundos;
        
        $input = $request->all();//muestra todos los registros en un array

        $input['duration'] = $duration;//agrega elemento al array

        return $input;
    }
}
