<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Artist;
use App\Models\Band;

class Country extends Model
{
    use HasFactory;

    protected $table = 'countries';

    protected $fillable = ['name','country_id'];

    public function artists()
    {
        return $this->hasMany(Artist::class, 'country_id');
    }

    public function bands()
    {
        return $this->hasMany(Band::class, 'country_id');
    }


}
