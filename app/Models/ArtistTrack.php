<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtistTrack extends Model
{
    use HasFactory;

    protected $table = 'artist_track';

    protected $fillable = ['artist_id', 'track_id', 'participation_id'];

# RELATIONSHIPS

    public function relArtist()
    {
        return $this->belongsTo(Artist::class,'artist_id');
    }

    public function relTrack()
    {
        return $this->belongsTo(Track::class, 'track_id');
    }

    public function relParticipation()
    {
        return $this->belongsTo(Participation::class, 'participation_id');
    }
}
