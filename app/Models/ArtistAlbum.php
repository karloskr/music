<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ArtistAlbum extends Model
{
    use HasFactory;

    protected $table = 'artist_album';

    protected $fillable = ['artist_id', 'album_id','participation_id'];


    public function relArtist()
    {
        return $this->belongsTo(Artist::class,'artist_id');
    }

    public function relAlbum()
    {
        return $this->belongsTo(Album::class,'album_id');
    }

    public function relParticipation()
    {
        return $this->belongsTo(Participation::class,'participation_id');
    }
}
