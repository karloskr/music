<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Album;
use App\Models\Country;
use App\Models\Artist;

class Band extends Model
{
    use HasFactory;

    protected $table = 'bands';

    protected $fillable = ['name','start','end','history','imagen','country_id','gender_id'];

    public function artists()
    {
        return $this->belongsToMany(Artist::class, 'members', 'artist_id', 'band_id')->withTimestamps();
    }

    public function albums()
    {
        return $this->hasMany(Album::class, 'band_id');
    }

    public function country()
    {
        return $this->belongsTo(Country::class,'country_id');
    }

    public function members()
    {
        return $this->hasMany(Member::class, 'band_id');
    }

    public function image()
    {
        return $this->morphOne('App\Models\Image', 'imageable');
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class,'gender_id');
    }

}
