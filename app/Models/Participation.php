<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Participation extends Model
{
    use HasFactory;

    protected $table = 'participations';

    protected $fillable = ['name'];

    public function relArtistsTracks()
    {
        return $this->hasMany(ArtistTrack::class, 'participation_id');
    }

    public function relArtistAlbum()
    {
        return $this->hasMany(ArtistAlbum::class,'participation_id');
    }

}
