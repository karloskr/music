<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Track extends Model
{
    use HasFactory;

    protected $table = 'tracks';

    protected $fillable = ['name','duration','album_id', 'style_id'];

    protected $appends = ['duration_format'];  //Appending Values To JSON

    public function artists()
    {
        return $this->belongsToMany(Artist::class, 'artist_track', 'artist_id','track_id', 'partipation_id')->withTimestamps();
    }

    public function style()
    {
        return $this->belongsTo(Style::class, 'style_id');
    }

    public function albums()
    {
        return $this->belongsTo(Album::class, 'album_id');
    }

    public function relArtistsTracks()
    {
        return $this->hasMany(ArtistTrack::class, 'track_id');
    }

    /* public function setDurationAttribute($duration)
    {
        $this->attributes['duration'] = ($duration);
    } */

    public function getDurationFormatAttribute() //Accesor
    {
        return date('H:i:s', $this->attributes['duration']);
    }


}
